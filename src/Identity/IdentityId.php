<?php

namespace Glance\AuthorizationService\Identity;

use Glance\AuthorizationService\Shared\Uuid;

final class IdentityId extends Uuid
{
}
