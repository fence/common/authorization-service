<?php

namespace Glance\AuthorizationService\Identity;

use InvalidArgumentException;

/**
 * Enum for identity type
 */
final class IdentityType
{
    /** @var string */
    private $type;

    /** @var string[] */
    public static $allowedTypes = [
        "Person",
        "Service",
        "Secondary",
        "Application",
        "Unknown"
    ];

    private function __construct(string $type)
    {
        if (!in_array($type, self::$allowedTypes)) {
            $allowed = implode(", ", self::$allowedTypes);
            throw new InvalidArgumentException(
                "Identity type should be one of those values: {$allowed}"
            );
        }

        $this->type = $type;
    }

    public static function person(): self
    {
        return new self("Person");
    }

    public static function service(): self
    {
        return new self("Service");
    }

    public static function secondary(): self
    {
        return new self("Secondary");
    }

    public static function application(): self
    {
        return new self("Application");
    }

    public static function unknown(): self
    {
        return new self("Unknown");
    }

    /** @internal */
    public static function fromString(string $type): self
    {
        return new self($type);
    }

    public function toString(): string
    {
        return $this->type;
    }
}
