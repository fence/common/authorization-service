<?php

namespace Glance\AuthorizationService\Identity\Exception;

use Exception;

class FailedToSearchIdentityException extends Exception
{
}
