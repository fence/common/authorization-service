<?php

namespace Glance\AuthorizationService\Identity\Exception;

use Exception;

class UpnNotFoundException extends Exception
{
}
