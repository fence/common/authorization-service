<?php

namespace Glance\AuthorizationService\Identity\Exception;

use Exception;

class PersonIdNotFoundException extends Exception
{
}
