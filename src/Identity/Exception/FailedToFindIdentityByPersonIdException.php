<?php

namespace Glance\AuthorizationService\Identity\Exception;

use Exception;

class FailedToFindIdentityByPersonIdException extends Exception
{
}
