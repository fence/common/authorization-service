<?php

namespace Glance\AuthorizationService\Identity\Exception;

use Exception;

class FailedToFindAllIdentitiesException extends Exception
{
}
