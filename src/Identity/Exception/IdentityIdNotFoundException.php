<?php

namespace Glance\AuthorizationService\Identity\Exception;

use Exception;

class IdentityIdNotFoundException extends Exception
{
}
