<?php

namespace Glance\AuthorizationService\Identity\Exception;

use Exception;

class FailedToFindPersonByIdentityIdException extends Exception
{
}
