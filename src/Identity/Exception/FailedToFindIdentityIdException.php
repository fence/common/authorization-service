<?php

namespace Glance\AuthorizationService\Identity\Exception;

use Exception;

class FailedToFindIdentityIdException extends Exception
{
}
