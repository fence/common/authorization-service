<?php

namespace Glance\AuthorizationService\Identity\Exception;

use Exception;

class FailedToFindIdentityByUpnException extends Exception
{
}
