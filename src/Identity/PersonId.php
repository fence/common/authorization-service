<?php

namespace Glance\AuthorizationService\Identity;

class PersonId
{
    /** @var int */
    private $id;

    private function __construct(int $id)
    {
        $this->id = $id;
    }

    public static function fromInteger(int $id): self
    {
        return new self($id);
    }

    public function toInteger(): int
    {
        return $this->id;
    }
}
