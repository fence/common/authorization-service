<?php

namespace Glance\AuthorizationService\Identity;

/**
 * @psalm-type IdentityArray = array{
 *     id: string,
 *     type: string,
 *     upn: string|null,
 *     displayName: string,
 *     personId: string|null,
 *     lastName: string|null,
 *     firstName: string|null,
 *     disabled: bool|null,
 *     blockingReason: string|null,
 * }
 */
class Identity
{
    /** @var IdentityId */
    private $id;

    /** @var IdentityType */
    private $type;

    /** @var string|null */
    private $upn;

    /** @var string */
    private $displayName;

    /** @var PersonId|null */
    private $personId;

    /** @var string|null */
    private $lastName;

    /** @var string|null */
    private $firstName;

    /** @var bool|null */
    private $blocked;

    /** @var string|null */
    private $blockingReason;

    private function __construct(
        IdentityId $id,
        IdentityType $type,
        ?string $upn,
        string $displayName,
        ?PersonId $personId,
        ?string $lastName,
        ?string $firstName,
        ?bool $blocked,
        ?string $blockingReason
    ) {
        $this->id = $id;
        $this->type = $type;
        $this->upn = $upn;
        $this->displayName = $displayName;
        $this->personId = $personId;
        $this->lastName = $lastName;
        $this->firstName = $firstName;
        $this->blocked = $blocked;
        $this->blockingReason = $blockingReason;
    }

    /** @psalm-param IdentityArray $array */
    public static function fromArray(array $array): self
    {
        $personId = $array["personId"] ?
            PersonId::fromInteger((int) $array["personId"]) : null;

        $blocked = isset($array["disabled"]) && $array["disabled"] === true ? true : false;

        return new self(
            IdentityId::fromString($array["id"]),
            IdentityType::fromString($array["type"]),
            $array["upn"],
            $array["displayName"],
            $personId,
            $array["lastName"] ?? null,
            $array["firstName"] ?? null,
            $blocked,
            $array["blockingReason"] ?? null
        );
    }

    /** @psalm-return IdentityArray */
    public function toArray(): array
    {
        $personId = $this->personId ? (string) $this->personId->toInteger() : null;
        return [
            "id"              => $this->id->toString(),
            "type"            => $this->type->toString(),
            "upn"             => $this->upn,
            "displayName"     => $this->displayName,
            "personId"        => $personId,
            "lastName"        => $this->lastName,
            "firstName"       => $this->firstName,
            "disabled"         => $this->blocked,
            "blockingReason"   => $this->blockingReason
        ];
    }

    public function personId(): ?PersonId
    {
        return $this->personId;
    }

    public function id(): IdentityId
    {
        return $this->id;
    }

    public function type(): IdentityType
    {
        return $this->type;
    }

    public function upn(): ?string
    {
        return $this->upn;
    }

    public function displayName(): string
    {
        return $this->displayName;
    }

    public function firstName(): ?string
    {
        return $this->firstName;
    }

    public function lastName(): ?string
    {
        return $this->lastName;
    }

    public function blocked(): ?bool
    {
        return $this->blocked;
    }

    public function blockingReason(): ?string
    {
        return $this->blockingReason;
    }
}
