<?php

namespace Glance\AuthorizationService\Identity;

use Glance\AuthorizationService\Identity\Exception\FailedToFindAllIdentitiesException;
use Glance\AuthorizationService\Identity\Exception\FailedToFindIdentityByPersonIdException;
use Glance\AuthorizationService\Identity\Exception\FailedToFindIdentityByUpnException;
use Glance\AuthorizationService\Identity\Exception\FailedToFindIdentityIdException;
use Glance\AuthorizationService\Identity\Exception\FailedToSearchIdentityException;
use Glance\AuthorizationService\Identity\Exception\FailedToFindPersonByIdentityIdException;
use Glance\AuthorizationService\Identity\Exception\PersonIdNotFoundException;
use Glance\AuthorizationService\Identity\Exception\UpnNotFoundException;
use Glance\AuthorizationService\Identity\Exception\IdentityIdNotFoundException;
use Glance\AuthorizationService\Identity\IdentityId;
use Glance\AuthorizationService\Identity\Identity;
use Glance\AuthorizationService\RequestBuilder\Field;
use Glance\AuthorizationService\RequestBuilder\Filter;
use Glance\AuthorizationService\RequestBuilder\Limit;
use Glance\AuthorizationService\RequestBuilder\Offset;
use Glance\AuthorizationService\Shared\BaseProvider;
use GuzzleHttp\Promise;
use GuzzleHttp\Promise\PromiseInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @psalm-import-type IdentityArray from Identity
 */
class IdentityProvider extends BaseProvider
{
    /**
     * Get Identity IDs from multiple Person IDs
     *
     * If a Identity ID could not be found, a `FailedToFindIdentityIdException`
     * is thrown.
     *
     * @param int[] $personIds
     *
     * @return IdentityId[]
     *
     * @throws FailedToFindIdentityIdException
     */
    public function getIdsFromPersonIds(array $personIds): array
    {
        $requests = $this->idsFromPersonIdsRequest($personIds);
        $responseHandler = $this->idsFromPersonIdsResponseHandler();
        $throwOnError = true;

        return $this->resolveIdsFromPersonIdsRequests($requests, $responseHandler, $throwOnError);
    }

    /** @param int[] $personIds */
    public function findIdsFromPersonIds(array $personIds): array
    {
        $requests = $this->idsFromPersonIdsRequest($personIds);
        $responseHandler = $this->idsFromPersonIdsResponseHandler();
        $throwOnError = false;

        return $this->resolveIdsFromPersonIdsRequests($requests, $responseHandler, $throwOnError);
    }

    /**
     * Should be used when $personIds' length is greater than 300
     *
     * @param int[] $personIds
     *
     * @throws FailedToFindAllIdentitiesException
     */
    public function findIdsFromManyPersonIds(array $personIds, ?IdentityType $type = null): array
    {
        if ($type === null) {
            $type = IdentityType::person();
        }

        $paginationLimit = 1000;
        $idMapper = [];

        $page = 0;
        do {
            $paginationOffset = $paginationLimit * $page;
            $request = $this->identityApi->getIdentities(
                [
                    Field::fromString("id"),
                    Field::fromString("personId"),
                    Filter::equals("type", $type->toString()),
                    Filter::equals("source", "cern"),
                    Limit::fromInteger($paginationLimit),
                    Offset::fromInteger($paginationOffset)
                ],
                $this->verifiedAccessToken()
            );

            $response = $this->client->sendRequest($request);

            if ($response->getStatusCode() >= 400) {
                /** @psalm-var array{ message?: string }|null $responseBody */
                $responseBody = json_decode((string) $response->getBody(), true);

                $reason = null;
                if ($responseBody !== null && array_key_exists("message", $responseBody)) {
                    $reason = $responseBody["message"];
                }

                throw new FailedToFindAllIdentitiesException(
                    "Failed to load identities. Reason: $reason"
                );
            }

            /** @psalm-var array{ data: array<int, array{ id: string, personId: string }> } $body */
            $body = json_decode((string) $response->getBody(), true);
            $fetchedIdentities = $body["data"];
            foreach ($fetchedIdentities as $identityArray) {
                if (isset($identityArray["personId"])) {
                    $personId = (int) $identityArray["personId"];
                    $idMapper[$personId] = IdentityId::fromString($identityArray["id"]);
                }
            }

            $page++;
        } while (!empty($fetchedIdentities));

        $result = [];
        foreach ($personIds as $personId) {
            $result[$personId] = $idMapper[$personId] ?? null;
        }

        return $result;
    }

    /**
     * Find Identity IDs from multiple Person IDs
     *
     * If the Identity ID is not found, it will NOT be included on the returned
     * array and no exceptions will be thrown.
     *
     * @param int[] $personIds
     *
     * @return RequestInterface[]
     */
    private function idsFromPersonIdsRequest(array $personIds): array
    {
        return array_map(
            function (int $personId): RequestInterface {
                return $this->identityApi->getIdentities(
                    [
                        Filter::equals("personId", (string) $personId),
                        Filter::equals("source", "cern"),
                        Field::fromString("id"),
                    ],
                    $this->verifiedAccessToken()
                );
            },
            $personIds
        );
    }

    /** @return callable(ResponseInterface): IdentityId */
    private function idsFromPersonIdsResponseHandler(): callable
    {
        return function (ResponseInterface $response): IdentityId {
            if ($response->getStatusCode() >= 400) {
                /** @psalm-var array{ message?: string }|null $responseBody */
                $responseBody = json_decode((string) $response->getBody(), true);

                $reason = null;
                if ($responseBody !== null && array_key_exists("message", $responseBody)) {
                    $reason = $responseBody["message"];
                }

                throw new FailedToFindIdentityIdException(
                    "Failed find identity ID. Reason: '{$reason}'"
                );
            }

            /** @psalm-var array{ data: array<int, array{ id: string }> } $body */
            $body = json_decode((string) $response->getBody(), true);

            if (empty($body["data"])) {
                throw new FailedToFindIdentityIdException(
                    "Failed find identity ID with given personId"
                );
            }

            return IdentityId::fromString($body["data"][0]["id"]);
        };
    }

    /**
     * @param RequestInterface[] $requests
     * @param callable(ResponseInterface): IdentityId $responseHandler
     *
     * @return IdentityId[]
     */
    private function resolveIdsFromPersonIdsRequests(
        array $requests,
        callable $responseHandler,
        bool $throwOnError
    ): array {
        // With the Guzzle client, we can make async and concurrent requests
        if ($this->client instanceof \GuzzleHttp\ClientInterface) {
            $promises = array_map(
                function (RequestInterface $request): PromiseInterface {
                    /** @var \GuzzleHttp\ClientInterface $client */
                    $client = $this->client;
                    return $client->sendAsync($request);
                },
                $requests
            );

            /**
             * @var array<int, array{
             *      value?: \Psr\Http\Message\ResponseInterface,
             *      reason?: \GuzzleHttp\Exception\RequestException
             * }> $promiseResults
             */
            $promiseResults = Promise\Utils::settle($promises)->wait();

            $ids = array_map(
                function (array $result) use ($responseHandler, $throwOnError) {
                    if (array_key_exists("value", $result)) {
                        return $responseHandler($result["value"]);
                    }

                    if (array_key_exists("reason", $result) && $throwOnError) {
                        $response = $result["reason"]->getResponse();
                        if ($response !== null) {
                            return $responseHandler($response);
                        }
                    }

                    return;
                },
                $promiseResults
            );

            // Clean empty results
            return array_values(array_filter($ids));
        }

        $ids = array_map(
            function (RequestInterface $request) use ($responseHandler): ?IdentityId {
                $response = $this->client->sendRequest($request);

                try {
                    return $responseHandler($response);
                } catch (\Throwable $e) {
                    return null;
                }
            },
            $requests
        );

        // Clean empty results
        return array_values(array_filter($ids));
    }

    /**
     * Get Identity Details that contain $lookupText
     *
     * @param string $lookupText
     *
     * @return Identity[]
     */
    public function searchByName(string $lookupText, ?IdentityType $type = null): array
    {
        if ($type === null) {
            $type = IdentityType::person();
        }

        $request = $this->identityApi->getIdentities(
            [
                Filter::equals("source", "cern"),
                Filter::fromStrings("displayName", $lookupText, "contains"),
                Filter::equals("type", $type->toString()),
                Field::fromString("id"),
                Field::fromString("type"),
                Field::fromString("upn"),
                Field::fromString("displayName"),
                Field::fromString("personId"),
                Field::fromString("lastName"),
                Field::fromString("firstName"),
                Field::fromString("disabled"),
                Field::fromString("blockingReason")
            ],
            $this->verifiedAccessToken()
        );

        $response = $this->client->sendRequest($request);

        if ($response->getStatusCode() >= 400) {
            /** @psalm-var array{ message?: string }|null $responseBody */
            $responseBody = json_decode((string) $response->getBody(), true);

            $reason = null;
            if ($responseBody !== null && array_key_exists("message", $responseBody)) {
                $reason = $responseBody["message"];
            }

            throw new FailedToSearchIdentityException(
                "Failed to search identity: '{$lookupText}'. Reason: '{$reason}'"
            );
        }

        /** @psalm-var array{ data: IdentityArray[] } $responseBody */
        $responseBody = json_decode((string) $response->getBody(), true);

        return array_map(
            function (array $identity): Identity {
                return Identity::fromArray($identity);
            },
            $responseBody["data"]
        );
    }

    public function findByUpn(string $upn, ?IdentityType $type = null): Identity
    {
        if ($type === null) {
            $type = IdentityType::person();
        }

        $request = $this->identityApi->getIdentities(
            [
                Filter::equals("source", "cern"),
                Filter::equals("upn", $upn),
                Filter::equals("type", $type->toString()),
                Field::fromString("id"),
                Field::fromString("type"),
                Field::fromString("upn"),
                Field::fromString("displayName"),
                Field::fromString("personId"),
                Field::fromString("lastName"),
                Field::fromString("firstName"),
                Field::fromString("disabled"),
                Field::fromString("blockingReason")
            ],
            $this->verifiedAccessToken()
        );

        $response = $this->client->sendRequest($request);

        if ($response->getStatusCode() >= 400) {
            /** @psalm-var array{ message?: string }|null $responseBody */
            $responseBody = json_decode((string) $response->getBody(), true);

            $reason = null;
            if ($responseBody !== null && array_key_exists("message", $responseBody)) {
                $reason = $responseBody["message"];
            }

            throw new FailedToFindIdentityByUpnException(
                "Failed to find identity for '{$upn}'. Reason: '{$reason}'"
            );
        }

        /** @psalm-var array{
         *      pagination: array{ total: int },
         *      data: IdentityArray[]
         * } $responseBody */
        $responseBody = json_decode((string) $response->getBody(), true);
        if ($responseBody["pagination"]["total"] === 0) {
            throw new UpnNotFoundException("Identity not found with upn: '{$upn}'");
        }

        return Identity::fromArray($responseBody["data"][0]);
    }

    /**
     * Should be used when $upns' length is greater than 300
     *
     * @param string[] $upns
     *
     * @throws FailedToFindAllIdentitiesException
     */
    public function findIdsFromManyUpns(array $upns, ?IdentityType $type = null): array
    {
        if ($type === null) {
            $type = IdentityType::person();
        }

        $paginationLimit = 5000;
        $idMapper = [];

        $page = 0;
        do {
            $paginationOffset = $paginationLimit * $page;
            $request = $this->identityApi->getIdentities(
                [
                    Field::fromString("id"),
                    Field::fromString("upn"),
                    Filter::equals("type", $type->toString()),
                    Limit::fromInteger($paginationLimit),
                    Offset::fromInteger($paginationOffset)
                ],
                $this->verifiedAccessToken()
            );

            $response = $this->client->sendRequest($request);

            if ($response->getStatusCode() >= 400) {
                /** @psalm-var array{ message?: string }|null $responseBody */
                $responseBody = json_decode((string) $response->getBody(), true);

                $reason = null;
                if ($responseBody !== null && array_key_exists("message", $responseBody)) {
                    $reason = $responseBody["message"];
                }

                throw new FailedToFindAllIdentitiesException(
                    "Failed to load identities. Reason: $reason"
                );
            }

            /** @psalm-var array{ data: array<int, array{ id: string, upn: string }> } $body */
            $body = json_decode((string) $response->getBody(), true);
            $fetchedIdentities = $body["data"];
            foreach ($fetchedIdentities as $identityArray) {
                if (isset($identityArray["upn"])) {
                    $upn = $identityArray["upn"];
                    $idMapper[$upn] = IdentityId::fromString($identityArray["id"]);
                }
            }

            $page++;
        } while (!empty($fetchedIdentities));

        $result = [];
        foreach ($upns as $upn) {
            $result[$upn] = $idMapper[$upn] ?? null;
        }

        return $result;
    }

    public function findByPersonId(int $personId, ?IdentityType $type = null): Identity
    {
        if ($type === null) {
            $type = IdentityType::person();
        }

        $request = $this->identityApi->getIdentities(
            [
                Filter::equals("source", "cern"),
                Filter::equals("personId", "$personId"),
                Filter::equals("type", $type->toString()),
                Field::fromString("id"),
                Field::fromString("type"),
                Field::fromString("upn"),
                Field::fromString("displayName"),
                Field::fromString("personId"),
                Field::fromString("lastName"),
                Field::fromString("firstName"),
                Field::fromString("disabled"),
                Field::fromString("blockingReason")
            ],
            $this->verifiedAccessToken()
        );

        $response = $this->client->sendRequest($request);

        if ($response->getStatusCode() >= 400) {
            /** @psalm-var array{ message?: string }|null $responseBody */
            $responseBody = json_decode((string) $response->getBody(), true);

            $reason = null;
            if ($responseBody !== null && array_key_exists("message", $responseBody)) {
                $reason = $responseBody["message"];
            }

            throw new FailedToFindIdentityByPersonIdException(
                "Failed to find identity for '{$personId}'. Reason: '{$reason}'"
            );
        }

        /** @psalm-var array{
         *      pagination: array{ total: int },
         *      data: IdentityArray[]
         * } $responseBody */
        $responseBody = json_decode((string) $response->getBody(), true);
        if ($responseBody["pagination"]["total"] === 0) {
            throw new PersonIdNotFoundException("Identity not found with upn: '{$personId}'");
        }

        return Identity::fromArray($responseBody["data"][0]);
    }

    public function findByIdentityId(string $identityId, ?IdentityType $type = null): Identity
    {
        if ($type === null) {
            $type = IdentityType::person();
        }

        $request = $this->identityApi->getIdentities(
            [
                Filter::equals("source", "cern"),
                Filter::equals("id", "$identityId"),
                Filter::equals("type", $type->toString()),
                Field::fromString("id"),
                Field::fromString("type"),
                Field::fromString("upn"),
                Field::fromString("displayName"),
                Field::fromString("personId"),
                Field::fromString("lastName"),
                Field::fromString("firstName"),
                Field::fromString("disabled"),
                Field::fromString("blockingReason")
            ],
            $this->verifiedAccessToken()
        );

        $response = $this->client->sendRequest($request);

        if ($response->getStatusCode() >= 400) {
            /** @psalm-var array{ message?: string }|null $responseBody */
            $responseBody = json_decode((string) $response->getBody(), true);

            $reason = null;
            if ($responseBody !== null && array_key_exists("message", $responseBody)) {
                $reason = $responseBody["message"];
            }

            throw new FailedToFindPersonByIdentityIdException(
                "Failed to find person for '{$identityId}'. Reason: '{$reason}'"
            );
        }

        /** @psalm-var array{
         *      pagination: array{ total: int },
         *      data: IdentityArray[]
         * } $responseBody */
        $responseBody = json_decode((string) $response->getBody(), true);
        if ($responseBody["pagination"]["total"] === 0) {
            throw new IdentityIdNotFoundException("Person not found: '{$identityId}'");
        }

        return Identity::fromArray($responseBody["data"][0]);
    }

    /**
     * Should be used when $personIds' length is greater than 300 and you
     * need to filter disabled identities
     * @param int[] $personIds
     *
     * @throws FailedToFindAllIdentitiesException
     */
    public function findUnblockedIdsFromManyPersonIds(array $personIds): array
    {
        $paginationLimit = 1000;
        $idMapper = [];

        $page = 0;
        do {
            $paginationOffset = $paginationLimit * $page;
            $request = $this->identityApi->getIdentities(
                [
                    Field::fromString("id"),
                    Field::fromString("personId"),
                    Filter::equals("type", "Person"),
                    Filter::notEquals("disabled", "true"),
                    Filter::equals("source", "cern"),
                    Limit::fromInteger($paginationLimit),
                    Offset::fromInteger($paginationOffset)
                ],
                $this->verifiedAccessToken()
            );

            $response = $this->client->sendRequest($request);

            if ($response->getStatusCode() >= 400) {
                /** @psalm-var array{ message?: string }|null $responseBody */
                $responseBody = json_decode((string) $response->getBody(), true);

                $reason = null;
                if ($responseBody !== null && array_key_exists("message", $responseBody)) {
                    $reason = $responseBody["message"];
                }

                throw new FailedToFindAllIdentitiesException(
                    "Failed to load identities. Reason: $reason"
                );
            }

            /** @psalm-var array{ data: array<int, array{ id: string, personId: string }> } $body */
            $body = json_decode((string) $response->getBody(), true);
            $fetchedIdentities = $body["data"];
            foreach ($fetchedIdentities as $identityArray) {
                if (isset($identityArray["personId"])) {
                    $personId = (int) $identityArray["personId"];
                    $idMapper[$personId] = IdentityId::fromString($identityArray["id"]);
                }
            }

            $page++;
        } while (!empty($fetchedIdentities));

        $result = [];
        foreach ($personIds as $personId) {
            $result[$personId] = $idMapper[$personId] ?? null;
        }

        return $result;
    }
}
