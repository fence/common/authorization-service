<?php

namespace Glance\AuthorizationService\Group;

use InvalidArgumentException;

/**
 * Enum for privacy policy
 */
final class PrivacyPolicy
{
    /** @var string */
    private $policy;

    /** @var string[] */
    public static $allowedPolicies = [
        "Admins",
        "Open",
        "Members",
    ];

    private function __construct(string $policy)
    {
        if (!in_array($policy, self::$allowedPolicies)) {
            $allowed = implode(", ", self::$allowedPolicies);
            throw new InvalidArgumentException(
                "Privacy policy should be one of those values: {$allowed}"
            );
        }

        $this->policy = $policy;
    }

    public static function groupAdministrators(): self
    {
        return new self("Admins");
    }

    public static function open(): self
    {
        return new self("Open");
    }

    public static function members(): self
    {
        return new self("Members");
    }

    public static function fromString(string $policy): self
    {
        return new self($policy);
    }

    public function toString(): string
    {
        return $this->policy;
    }
}
