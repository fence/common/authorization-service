<?php

namespace Glance\AuthorizationService\Group;

use InvalidArgumentException;

/**
 * Enum for self subscription policy
 */
final class SelfSubscriptionPolicy
{
    /** @var string */
    private $policy;

    /** @var string[] */
    public static $allowedPolicies = [
        "Closed",
        "Open",
        "CernUsers",
    ];

    private function __construct(string $policy)
    {
        if (!in_array($policy, self::$allowedPolicies)) {
            $allowed = implode(", ", self::$allowedPolicies);
            throw new InvalidArgumentException(
                "Self subscription policy should be one of those values: {$allowed}"
            );
        }

        $this->policy = $policy;
    }

    public static function closed(): self
    {
        return new self("Closed");
    }

    public static function open(): self
    {
        return new self("Open");
    }

    public static function cernUsers(): self
    {
        return new self("CernUsers");
    }

    public static function fromString(string $policy): self
    {
        return new self($policy);
    }

    public function toString(): string
    {
        return $this->policy;
    }
}
