<?php

namespace Glance\AuthorizationService\Group;

use InvalidArgumentException;

/**
 * Enum for Category
 */
final class Category
{
    /** @var string */
    private $category;

    /** @var string[] */
    public static $allowedCategories = [
        "Test",
        "Official",
        "Personal",
        "Undefined",
    ];

    private function __construct(string $category)
    {
        if (!in_array($category, self::$allowedCategories)) {
            $allowed = implode(", ", self::$allowedCategories);
            throw new InvalidArgumentException(
                "Category should be one of those values: {$allowed}"
            );
        }

        $this->category = $category;
    }

    public static function test(): self
    {
        return new self("Test");
    }

    public static function official(): self
    {
        return new self("Official");
    }

    public static function personal(): self
    {
        return new self("Personal");
    }

    public static function undefined(): self
    {
        return new self("Undefined");
    }

    public static function fromString(string $category): self
    {
        return new self($category);
    }

    public function toString(): string
    {
        return $this->category;
    }
}
