<?php

namespace Glance\AuthorizationService\Group;

use InvalidArgumentException;

final class SyncType
{
    /** @var string */
    private $syncType;

    /** @var string[] */
    public const ALLOWED_POLICIES = [
        "Replica",
        "Master",
        "NoSync",
        "SyncError"
    ];

    private function __construct(string $syncType)
    {
        $this->syncType = $syncType;
    }

    public static function replica(): self
    {
        return new self("Replica");
    }

    public static function master(): self
    {
        return new self("Master");
    }

    public static function noSync(): self
    {
        return new self("NoSync");
    }

    public static function syncError(): self
    {
        return new self("SyncError");
    }

    public static function fromString(string $syncType): self
    {
        return new self($syncType);
    }

    public function toString(): string
    {
        return $this->syncType;
    }
}
