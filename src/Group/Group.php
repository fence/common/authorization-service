<?php

namespace Glance\AuthorizationService\Group;

use Glance\AuthorizationService\Identity\IdentityId;

/**
 * @psalm-type GroupArray = array{
 *    id: string,
 *    groupIdentifier: string,
 *    displayName: string,
 *    description: string,
 *    ownerId: string|null,
 *    administratorsId: string|null,
 *    approvalRequired: bool,
 *    selfSubscriptionType: string,
 *    privacyType: string,
 *    dynamic: bool,
 *    syncType: string,
 *    resourceCategory: string
 * }
 */
class Group
{
    /** @var GroupId */
    private $id;

    /** @var string */
    private $groupIdentifier;

    /** @var string */
    private $displayName;

    /** @var string */
    private $description;

    /** @var IdentityId|null */
    private $ownerId;

    /** @var GroupId|null */
    private $administratorsId;

    /** @var bool */
    private $approvalRequired;

    /** @var SelfSubscriptionPolicy */
    private $selfSubscriptionType;

    /** @var PrivacyPolicy */
    private $privacyType;

    /** @var bool */
    private $dynamic;

    /** @var SyncType */
    private $syncType;

    /** @var Category */
    private $resourceCategory;

    private function __construct(
        GroupId $id,
        string $groupIdentifier,
        string $displayName,
        string $description,
        ?IdentityId $ownerId,
        ?GroupId $administratorsId,
        bool $approvalRequired,
        SelfSubscriptionPolicy $selfSubscriptionType,
        PrivacyPolicy $privacyType,
        bool $dynamic,
        SyncType $syncType,
        Category $resourceCategory
    ) {
        $this->id = $id;
        $this->groupIdentifier = $groupIdentifier;
        $this->displayName = $displayName;
        $this->description = $description;
        $this->ownerId = $ownerId;
        $this->administratorsId = $administratorsId;
        $this->approvalRequired = $approvalRequired;
        $this->selfSubscriptionType = $selfSubscriptionType;
        $this->privacyType = $privacyType;
        $this->dynamic = $dynamic;
        $this->syncType = $syncType;
        $this->resourceCategory = $resourceCategory;
    }

    /** @psalm-param GroupArray $array */
    public static function fromArray(array $array): self
    {
        $administratorsId = $array["administratorsId"] ?
            GroupId::fromString($array["administratorsId"]) : null;

        $ownerId = $array["ownerId"] ?
            IdentityId::fromString($array["ownerId"]) : null;

        return new self(
            GroupId::fromString($array["id"]),
            $array["groupIdentifier"],
            $array["displayName"],
            $array["description"],
            $ownerId,
            $administratorsId,
            $array["approvalRequired"],
            SelfSubscriptionPolicy::fromString($array["selfSubscriptionType"]),
            PrivacyPolicy::fromString($array["privacyType"]),
            $array["dynamic"],
            SyncType::fromString($array["syncType"]),
            Category::fromString($array["resourceCategory"])
        );
    }

    /** @psalm-return GroupArray */
    public function toArray(): array
    {
        $administratorsId = $this->administratorsId ? $this->administratorsId->toString() : null;
        $ownerId = $this->ownerId ? $this->ownerId->toString() : null;

        return [
            "id"                   => $this->id->toString(),
            "groupIdentifier"      => $this->groupIdentifier,
            "displayName"          => $this->displayName,
            "description"          => $this->description,
            "ownerId"              => $ownerId,
            "administratorsId"     => $administratorsId,
            "approvalRequired"     => $this->approvalRequired,
            "selfSubscriptionType" => $this->selfSubscriptionType->toString(),
            "privacyType"          => $this->privacyType->toString(),
            "dynamic"              => $this->dynamic,
            "syncType"             => $this->syncType->toString(),
            "resourceCategory"     => $this->resourceCategory->toString(),
        ];
    }

    public function id(): GroupId
    {
        return $this->id;
    }

    public function identifier(): string
    {
        return $this->groupIdentifier;
    }

    public function displayName(): string
    {
        return $this->displayName;
    }

    public function description(): string
    {
        return $this->description;
    }

    public function ownerId(): ?IdentityId
    {
        return $this->ownerId;
    }

    public function administratorsId(): ?GroupId
    {
        return $this->administratorsId;
    }

    public function approvalRequired(): bool
    {
        return $this->approvalRequired;
    }

    public function selfSubscriptionType(): SelfSubscriptionPolicy
    {
        return $this->selfSubscriptionType;
    }

    public function privacyType(): PrivacyPolicy
    {
        return $this->privacyType;
    }

    public function dynamic(): bool
    {
        return $this->dynamic;
    }

    public function syncType(): SyncType
    {
        return $this->syncType;
    }

    public function resourceCategory(): Category
    {
        return $this->resourceCategory;
    }
}
