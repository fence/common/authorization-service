<?php

namespace Glance\AuthorizationService\Group\Exception;

use Exception;

class FailedToAddMembersException extends Exception
{
}
