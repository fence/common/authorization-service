<?php

namespace Glance\AuthorizationService\Group\Exception;

use Exception;

class FailedToRemoveMembersException extends Exception
{
}
