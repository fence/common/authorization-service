<?php

namespace Glance\AuthorizationService\Group\Exception;

use Exception;

class FailedToSearchGroupException extends Exception
{
}
