<?php

namespace Glance\AuthorizationService\Group\Exception;

use Exception;

class FailedToCreateStaticGroupException extends Exception
{
}
