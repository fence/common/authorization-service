<?php

namespace Glance\AuthorizationService\Group\Exception;

use Exception;

class FailedToAddMemberGroupToGroupException extends Exception
{
}
