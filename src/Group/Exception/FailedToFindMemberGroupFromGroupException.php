<?php

namespace Glance\AuthorizationService\Group\Exception;

use Exception;

class FailedToFindMemberGroupFromGroupException extends Exception
{
}
