<?php

namespace Glance\AuthorizationService\Group\Exception;

use Exception;

class FailedToRemoveMemberGroupFromGroupException extends Exception
{
}
