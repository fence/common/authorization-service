<?php

namespace Glance\AuthorizationService\Group\Exception;

use Exception;

class FailedToDeleteGroupException extends Exception
{
}
