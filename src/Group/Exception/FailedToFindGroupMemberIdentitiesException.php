<?php

namespace Glance\AuthorizationService\Group\Exception;

use Exception;

class FailedToFindGroupMemberIdentitiesException extends Exception
{
}
