<?php

namespace Glance\AuthorizationService\Group\Exception;

use Exception;

class FailedToFindGroupException extends Exception
{
}
