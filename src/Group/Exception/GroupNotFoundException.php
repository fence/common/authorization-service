<?php

namespace Glance\AuthorizationService\Group\Exception;

use Exception;

class GroupNotFoundException extends Exception
{
}
