<?php

namespace Glance\AuthorizationService\Group\Exception;

use Exception;

class FailedToTransferGroupOwnership extends Exception
{
}
