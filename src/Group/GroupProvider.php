<?php

namespace Glance\AuthorizationService\Group;

use DateTimeImmutable;
use Glance\AuthorizationService\Group\Exception\FailedToAddMembersException;
use Glance\AuthorizationService\Group\Exception\FailedToCreateStaticGroupException;
use Glance\AuthorizationService\Group\Exception\FailedToDeleteGroupException;
use Glance\AuthorizationService\Group\Exception\FailedToFindGroupException;
use Glance\AuthorizationService\Group\Exception\FailedToFindGroupMemberIdentitiesException;
use Glance\AuthorizationService\Group\Exception\FailedToRemoveMembersException;
use Glance\AuthorizationService\Group\Exception\GroupNotFoundException;
use Glance\AuthorizationService\Group\Exception\FailedToFindMemberGroupFromGroupException;
use Glance\AuthorizationService\Group\Exception\FailedToRemoveMemberGroupFromGroupException;
use Glance\AuthorizationService\Group\Exception\FailedToSearchGroupException;
use Glance\AuthorizationService\Group\Exception\FailedToAddMemberGroupToGroupException;
use Glance\AuthorizationService\Group\Exception\FailedToTransferGroupOwnership;
use Glance\AuthorizationService\Identity\Identity;
use Glance\AuthorizationService\Identity\IdentityId;
use Glance\AuthorizationService\RequestBuilder\Filter;
use Glance\AuthorizationService\RequestBuilder\Limit;
use Glance\AuthorizationService\RequestBuilder\Offset;
use Glance\AuthorizationService\Shared\BaseProvider;
use Glance\AuthorizationService\RequestBuilder\Field;
use InvalidArgumentException;

/**
 * @psalm-import-type GroupArray from Group
 * @psalm-import-type IdentityArray from \Glance\AuthorizationService\Identity\Identity
 */
class GroupProvider extends BaseProvider
{
    public function searchByName(string $lookupText): array
    {
        $request = $this->groupApi->getGroups(
            [
                Filter::fromStrings("groupIdentifier", $lookupText, "contains"),
                Field::fromString("id"),
                Field::fromString("groupIdentifier"),
                Field::fromString("displayName"),
                Field::fromString("description"),
                Field::fromString("ownerId"),
                Field::fromString("administratorsId"),
                Field::fromString("approvalRequired"),
                Field::fromString("selfSubscriptionType"),
                Field::fromString("privacyType"),
                Field::fromString("dynamic"),
                Field::fromString("syncType"),
                Field::fromString("resourceCategory"),
            ],
            $this->verifiedAccessToken()
        );

        $response = $this->client->sendRequest($request);

        if ($response->getStatusCode() >= 400) {
            /** @psalm-var array{ message?: string }|null $responseBody */
            $responseBody = json_decode((string) $response->getBody(), true);

            $reason = null;
            if ($responseBody !== null && array_key_exists("message", $responseBody)) {
                $reason = $responseBody["message"];
            }

            throw new FailedToSearchGroupException(
                "Failed to search group: '{$lookupText}'. Reason: '{$reason}'"
            );
        }

        /** @psalm-var array{ data: GroupArray[] } $responseBody */
        $responseBody = json_decode((string) $response->getBody(), true);
        return array_map(
            function (array $group): Group {
                return Group::fromArray($group);
            },
            $responseBody["data"]
        );
    }

    public function findGroupByIdentifier(string $groupIdentifier): Group
    {
        $request = $this->groupApi->getGroups(
            [
                Filter::equals("groupIdentifier", $groupIdentifier),
            ],
            $this->verifiedAccessToken()
        );

        $response = $this->client->sendRequest($request);

        if ($response->getStatusCode() >= 400) {
            /** @psalm-var array{ message?: string }|null $responseBody */
            $responseBody = json_decode((string) $response->getBody(), true);

            $reason = null;
            if ($responseBody !== null && array_key_exists("message", $responseBody)) {
                $reason = $responseBody["message"];
            }

            throw new FailedToFindGroupException(
                "Failed to find group '{$groupIdentifier}'. Reason: '{$reason}'"
            );
        }

        /** @psalm-var array{
         *      pagination: array{ total: int },
         *      data: GroupArray[]
         * } $responseBody */
        $responseBody = json_decode((string) $response->getBody(), true);
        if ($responseBody["pagination"]["total"] === 0) {
            throw new GroupNotFoundException("Group not found: '{$groupIdentifier}'");
        }

        return Group::fromArray($responseBody["data"][0]);
    }

    public function createStaticGroup(
        string $groupIdentifier,
        string $displayName,
        string $description,
        ?GroupId $administratorsId = null,
        ?Category $category = null,
        ?DateTimeImmutable $expiration = null,
        ?SelfSubscriptionPolicy $selfSubscriptionPolicy = null,
        ?PrivacyPolicy $privacyPolicy = null,
        bool $approvalRequired = true
    ): Group {
        if (!$category) {
            $category = Category::test();
        }

        if (!$selfSubscriptionPolicy) {
            $selfSubscriptionPolicy = SelfSubscriptionPolicy::closed();
        }

        if (!$privacyPolicy) {
            $privacyPolicy = PrivacyPolicy::groupAdministrators();
        }

        $request = $this->groupApi->createGroup(
            [
                "groupIdentifier"      => $groupIdentifier,
                "displayName"          => $displayName,
                "description"          => $description,
                "administratorsId"     => $administratorsId ? $administratorsId->toString() : null,
                "resourceCategory"     => $category->toString(),
                "expirationDeadline"   => $expiration ? $expiration->format(DATE_ISO8601) : null,
                "selfSubscriptionType" => $selfSubscriptionPolicy->toString(),
                "privacyType"          => $privacyPolicy->toString(),
                "approvalRequired"     => $approvalRequired,
                "syncType"             => "Master",
            ],
            $this->verifiedAccessToken()
        );

        $response = $this->client->sendRequest($request);

        if ($response->getStatusCode() >= 400) {
            /** @psalm-var array{ message?: string }|null $responseBody */
            $responseBody = json_decode((string) $response->getBody(), true);

            $reason = null;
            if ($responseBody !== null && array_key_exists("message", $responseBody)) {
                $reason = $responseBody["message"];
            }

            throw new FailedToCreateStaticGroupException(
                "Failed to create group '{$groupIdentifier}'. Reason: '{$reason}'"
            );
        }

        /** @psalm-var array{ data: GroupArray } $responseBody */
        $responseBody = json_decode((string) $response->getBody(), true);

        return Group::fromArray($responseBody["data"]);
    }

    public function updateGroup(
        string $groupIdentifier,
        ?string $displayName,
        ?string $description,
        ?GroupId $administratorsId,
        ?Category $category,
        ?SelfSubscriptionPolicy $selfSubscriptionPolicy,
        ?PrivacyPolicy $privacyPolicy,
        ?bool $approvalRequired,
        ?SyncType $syncType
    ): Group {
        $group = $this->findGroupByIdentifier($groupIdentifier);
        $administratorsId = $administratorsId ?? $group->administratorsId();
        $category = $category ?? $group->resourceCategory();
        $selfSubscriptionPolicy = $selfSubscriptionPolicy !== null
            ? $selfSubscriptionPolicy : $group->selfSubscriptionType();
        $privacyPolicy = $privacyPolicy !== null ? $privacyPolicy : $group->privacyType();
        $approvalRequired = $approvalRequired !== null ? $approvalRequired : $group->approvalRequired();
        $syncType = $syncType !== null ? $syncType : $group->syncType();

        if (!in_array($syncType->toString(), SyncType::ALLOWED_POLICIES)) {
            $allowed = implode(", ", SyncType::ALLOWED_POLICIES);
            throw new InvalidArgumentException(
                "Sync type should be one of those values: {$allowed}. Given: {" . $syncType->toString() . "}"
            );
        }

        $request = $this->groupApi->updateGroup(
            $group->id()->toString(),
            [
                "groupIdentifier"      => $groupIdentifier,
                "displayName"          => $displayName ?? $group->displayName(),
                "description"          => $description ?? $group->description(),
                "administratorsId"     => $administratorsId ? $administratorsId->toString() : null,
                "resourceCategory"     => $category->toString(),
                "selfSubscriptionType" => $selfSubscriptionPolicy->toString(),
                "privacyType"          => $privacyPolicy->toString(),
                "approvalRequired"     => $approvalRequired,
                "syncType"             => $syncType->toString(),
            ],
            $this->verifiedAccessToken()
        );

        $response = $this->client->sendRequest($request);

        if ($response->getStatusCode() >= 400) {
            /** @psalm-var array{ message?: string }|null $responseBody */
            $responseBody = json_decode((string) $response->getBody(), true);

            $reason = null;
            if ($responseBody !== null && array_key_exists("message", $responseBody)) {
                $reason = $responseBody["message"];
            }

            throw new FailedToCreateStaticGroupException(
                "Failed to update group '{$groupIdentifier}'. Reason: '{$reason}'"
            );
        }

        /** @psalm-var array{ data: GroupArray } $responseBody */
        $responseBody = json_decode((string) $response->getBody(), true);

        return Group::fromArray($responseBody["data"]);
    }

    public function deleteGroup(string $groupIdentifier): void
    {
        $group = $this->findGroupByIdentifier($groupIdentifier);

        $request = $this->groupApi->deleteGroup(
            $group->id()->toString(),
            $this->verifiedAccessToken()
        );

        $response = $this->client->sendRequest($request);

        if ($response->getStatusCode() >= 400) {
            /** @psalm-var array{ message?: string }|null $responseBody */
            $responseBody = json_decode((string) $response->getBody(), true);

            $reason = null;
            if ($responseBody !== null && array_key_exists("message", $responseBody)) {
                $reason = $responseBody["message"];
            }

            throw new FailedToDeleteGroupException(
                "Failed to delete '{$groupIdentifier}'. Reason: '{$reason}'"
            );
        }
    }

    /**  @param IdentityId[] $identityIds */
    public function synchronizeMembers(string $groupIdentifier, array $identityIds): void
    {
        $currentIdentityIds = array_map(
            function (Identity $identity) {
                return $identity->id();
            },
            $this->findMemberIdentities($groupIdentifier)
        );

        $addedIdentityIds = IdentityId::diff($identityIds, $currentIdentityIds);
        $removedIdentityIds = IdentityId::diff($currentIdentityIds, $identityIds);

        $this->addMembersToGroup($groupIdentifier, $addedIdentityIds);
        $this->removeMembersFromGroup($groupIdentifier, $removedIdentityIds);
    }

    /** @param IdentityId[] $identityIds */
    public function addMembersToGroup(string $groupIdentifier, array $identityIds): void
    {
        $group = $this->findGroupByIdentifier($groupIdentifier);
        $groupId = $group->id();

        /** Prevent bad requests due to query string limit length */
        $idChunks = array_chunk($identityIds, 100);
        foreach ($idChunks as $idChunk) {
            $request = $this->groupApi->addMembersToGroup(
                $groupId->toString(),
                IdentityId::toStrings($idChunk),
                $this->verifiedAccessToken()
            );
            $response = $this->client->sendRequest($request);

            if ($response->getStatusCode() >= 400) {
                /** @psalm-var array{ message?: string }|null $responseBody */
                $responseBody = json_decode((string) $response->getBody(), true);

                $reason = null;
                if ($responseBody !== null && array_key_exists("message", $responseBody)) {
                    $reason = $responseBody["message"];
                }

                throw new FailedToAddMembersException(
                    "Failed to add members to '{$groupIdentifier}'. Reason: '{$reason}'"
                );
            }
        }
    }

    /**
     * @param IdentityId[] $identityIds
     */
    public function removeMembersFromGroup(string $groupIdentifier, array $identityIds): void
    {
        $group = $this->findGroupByIdentifier($groupIdentifier);
        $groupId = $group->id();

        /** Prevent bad requests due to query string limit length */
        $idChunks = array_chunk($identityIds, 100);
        foreach ($idChunks as $idChunk) {
            $request = $this->groupApi->removeMembersFromGroup(
                $groupId->toString(),
                IdentityId::toStrings($idChunk),
                $this->verifiedAccessToken()
            );
            $response = $this->client->sendRequest($request);

            if ($response->getStatusCode() >= 400) {
                /** @psalm-var array{ message?: string }|null $responseBody */
                $responseBody = json_decode((string) $response->getBody(), true);

                $reason = null;
                if ($responseBody !== null && array_key_exists("message", $responseBody)) {
                    $reason = $responseBody["message"];
                }

                throw new FailedToRemoveMembersException(
                    "Failed to remove members from '{$groupIdentifier}'. Reason: '{$reason}'"
                );
            }
        }
    }

    /**
     * Return the members of $groupIdentifier.
     * @return Identity[]
     */
    public function findMemberIdentities(string $groupIdentifier): array
    {
        $group = $this->findGroupByIdentifier($groupIdentifier);
        $groupId = $group->id();

        $request = $this->groupApi->getGroupMembers(
            $groupId->toString(),
            [
                Limit::fromInteger(100000)
            ],
            $this->verifiedAccessToken()
        );

        $response = $this->client->sendRequest($request);

        if ($response->getStatusCode() >= 400) {
            /** @psalm-var array{ message?: string }|null $responseBody */
            $responseBody = json_decode((string) $response->getBody(), true);

            $reason = null;
            if ($responseBody !== null && array_key_exists("message", $responseBody)) {
                $reason = $responseBody["message"];
            }

            throw new FailedToFindGroupMemberIdentitiesException(
                "Failed to remove members from '{$groupIdentifier}'. Reason: '{$reason}'"
            );
        }

        /** @psalm-var array{ data: IdentityArray[] } $responseBody */
        $responseBody = json_decode((string) $response->getBody(), true);

        return array_map(
            function (array $identity): Identity {
                return Identity::fromArray($identity);
            },
            $responseBody["data"]
        );
    }

    /**
     *  Return the members of $groupIdentifier and its subgroups.
     *  @return Identity[]
     */
    public function findMemberIdentitiesRecursive(string $groupIdentifier): array
    {
        $group = $this->findGroupByIdentifier($groupIdentifier);
        $identities = [];
        $page = 0;

        do {
            $paginationOffset = Limit::MAXIMUM_LIMIT * $page;
            $request = $this->groupApi->getGroupMembersRecursive(
                $group->id()->toString(),
                [
                    Limit::fromInteger(Limit::MAXIMUM_LIMIT),
                    Offset::fromInteger($paginationOffset)
                ],
                $this->verifiedAccessToken()
            );

            $response = $this->client->sendRequest($request);

            if ($response->getStatusCode() >= 400) {
                /** @psalm-var array{ message?: string }|null $responseBody */
                $responseBody = json_decode((string) $response->getBody(), true);

                $reason = null;
                if ($responseBody !== null && array_key_exists("message", $responseBody)) {
                    $reason = $responseBody["message"];
                }

                throw new FailedToFindGroupMemberIdentitiesException(
                    "Failed to fetch '{$groupIdentifier}' members. Reason: '{$reason}'"
                );
            }

            /** @psalm-var array{ data: IdentityArray[] } $responseBody */
            $responseBody = json_decode((string) $response->getBody(), true);
            $fetchedIdentities = $responseBody["data"];
            foreach ($fetchedIdentities as $identityArray) {
                $identities[] = Identity::fromArray($identityArray);
            }

            $page++;
        } while (!empty($fetchedIdentities));

        return $identities;
    }

    /**
     * Gets the group member groups
     * Return the groups of $groupIdentifier.
     * @return Group[]
     */
    public function findMemberGroupsFromGroup(string $groupIdentifier): array
    {
        $group = $this->findGroupByIdentifier($groupIdentifier);
        $groupId = $group->id();

        $request = $this->groupApi->getGroupMemberGroups(
            $groupId->toString(),
            [
                Limit::fromInteger(100000)
            ],
            $this->verifiedAccessToken()
        );

        $response = $this->client->sendRequest($request);

        if ($response->getStatusCode() >= 400) {
            /** @psalm-var array{ message?: string }|null $responseBody */
            $responseBody = json_decode((string) $response->getBody(), true);

            $reason = null;
            if ($responseBody !== null && array_key_exists("message", $responseBody)) {
                $reason = $responseBody["message"];
            }

            throw new FailedToFindMemberGroupFromGroupException(
                "Failed to find member groups from '{$groupIdentifier}'. Reason: '{$reason}'"
            );
        }

        /** @psalm-var array{ data: GroupArray[] } $responseBody */
        $responseBody = json_decode((string) $response->getBody(), true);

        return array_map(
            function (array $group): Group {
                return Group::fromArray($group);
            },
            $responseBody["data"]
        );
    }

    /**
     * Removes the group member groups
     * @param GroupId[] $groupIds
     */
    public function removeMemberGroupsFromGroup(string $groupIdentifier, array $groupIds): void
    {
        $group = $this->findGroupByIdentifier($groupIdentifier);
        $groupId = $group->id();

        /** Prevent bad requests due to query string limit length */
        $idChunks = array_chunk($groupIds, 100);
        foreach ($idChunks as $idChunk) {
            $request = $this->groupApi->removeMemberGroupsFromGroup(
                $groupId->toString(),
                GroupId::toStrings($idChunk),
                $this->verifiedAccessToken()
            );
            $response = $this->client->sendRequest($request);

            if ($response->getStatusCode() >= 400) {
                /** @psalm-var array{ message?: string }|null $responseBody */
                $responseBody = json_decode((string) $response->getBody(), true);

                $reason = null;
                if ($responseBody !== null && array_key_exists("message", $responseBody)) {
                    $reason = $responseBody["message"];
                }

                throw new FailedToRemoveMemberGroupFromGroupException(
                    "Failed to remove member groups from '{$groupIdentifier}'. Reason: '{$reason}'"
                );
            }
        }
    }

    public function removeAllMembersFromGroup(string $groupIdentifier): void
    {
        $identityIds = array_map(
            function (Identity $identity) {
                return $identity->id();
            },
            $this->findMemberIdentities($groupIdentifier)
        );

        $this->removeMembersFromGroup($groupIdentifier, $identityIds);
    }

    public function removeAllMemberGroupsFromGroup(string $groupIdentifier): void
    {
        $groupIds = array_map(
            function (Group $group) {
                return $group->id();
            },
            $this->findMemberGroupsFromGroup($groupIdentifier)
        );

        $this->removeMemberGroupsFromGroup($groupIdentifier, $groupIds);
    }

    public function removeAllMembersAndAllGroupMembersFromGroup(string $groupIdentifier): void
    {
        $this->removeAllMembersFromGroup($groupIdentifier);
        $this->removeAllMemberGroupsFromGroup($groupIdentifier);
    }

    /**
     * Adds member groups to group
     * @param GroupId[] $groupIds
     */
    public function addMemberGroupsToGroup(string $groupIdentifier, array $groupIds): void
    {
        $group = $this->findGroupByIdentifier($groupIdentifier);
        $groupId = $group->id();

        /** Prevent bad requests due to query string limit length */
        $idChunks = array_chunk($groupIds, 100);
        foreach ($idChunks as $idChunk) {
            $request = $this->groupApi->addMemberGroupsToGroup(
                $groupId->toString(),
                GroupId::toStrings($idChunk),
                $this->verifiedAccessToken()
            );
            $response = $this->client->sendRequest($request);

            if ($response->getStatusCode() >= 400) {
                /** @psalm-var array{ message?: string }|null $responseBody */
                $responseBody = json_decode((string) $response->getBody(), true);

                $reason = null;
                if ($responseBody !== null && array_key_exists("message", $responseBody)) {
                    $reason = $responseBody["message"];
                }

                throw new FailedToAddMemberGroupToGroupException(
                    "Failed to add groups to '{$groupIdentifier}'. Reason: '{$reason}'"
                );
            }
        }
    }

    public function transferGroupOwnership(string $groupIdentifier, string $newOwnerIdentifier): void
    {
        $group = $this->findGroupByIdentifier($groupIdentifier);
        $groupId = $group->id();

        $request = $this->groupApi->transferGroupOwnership(
            $groupId->toString(),
            $newOwnerIdentifier,
            $this->verifiedAccessToken()
        );

        $response = $this->client->sendRequest($request);

        if ($response->getStatusCode() >= 400) {
            /** @psalm-var array{ message?: string }|null $responseBody */
            $responseBody = json_decode((string) $response->getBody(), true);

            $reason = null;
            if ($responseBody !== null && array_key_exists("message", $responseBody)) {
                $reason = $responseBody["message"];
            }

            throw new FailedToTransferGroupOwnership(
                "Failed to transfer group ownership from '{$groupIdentifier}' to '{$newOwnerIdentifier}'.
                Reason: '{$reason}'"
            );
        }
    }
}
