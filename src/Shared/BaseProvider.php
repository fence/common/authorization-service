<?php

namespace Glance\AuthorizationService\Shared;

use Glance\AuthorizationService\RequestBuilder\GroupApi;
use Glance\AuthorizationService\RequestBuilder\IdentityApi;
use Glance\CernAuthentication\KeycloakProvider;
use GuzzleHttp\Client;
use Nyholm\Psr7\Factory\Psr17Factory;
use Psr\Http\Client\ClientInterface;

/**
 * @psalm-type OptionsArray = array{
 *     audienceId?: string,
 *     client?: ClientInterface,
 *     groupApi?: GroupApi,
 *     identityApi?: IdentityApi,
 *     keycloakProvider?: KeycloakProvider
 * }
 */
class BaseProvider
{
    /** @var string */
    protected $accessToken;

    /** @var ClientInterface */
    protected $client;

    /** @var GroupApi */
    protected $groupApi;

    /** @var IdentityApi */
    protected $identityApi;

    public const HOST_PRODUCTION = "authorization-service-api.web.cern.ch";
    public const HOST_QA = "authorization-service-api-qa.web.cern.ch";

    /** @psalm-param OptionsArray $options */
    final private function __construct(
        string $accessToken,
        array $options,
        bool $inProduction
    ) {
        $defaultHost = $inProduction ? self::HOST_PRODUCTION : self::HOST_QA;

        $defaultOptions = [
            "client"      => new Client(),
            "groupApi"    => GroupApi::createFromPsr17Factory(new Psr17Factory(), $defaultHost),
            "identityApi" => IdentityApi::createFromPsr17Factory(new Psr17Factory(), $defaultHost),
        ];

        $mergedOptions = array_merge($defaultOptions, $options);

        $this->accessToken = $accessToken;
        $this->client      = $mergedOptions["client"];
        $this->groupApi    = $mergedOptions["groupApi"];
        $this->identityApi = $mergedOptions["identityApi"];
    }

    /**
     * Used when you already have the access token.
     *
     * @psalm-param OptionsArray $options
     *
     * @return static
     */
    public static function createWithAccessToken(
        string $accessToken,
        array $options = [],
        bool $inProduction = true
    ): object {
        $inProduction = true;

        return new static($accessToken, $options, $inProduction);
    }

    /**
     * Used for performing operations as an application.
     *
     * @psalm-param OptionsArray $options
     *
     * @return static
     */
    public static function createWithAppCredentials(
        string $clientId,
        string $clientSecret,
        array $options = [],
        bool $inProduction = true
    ): self {

        $defaultOptions = [
            "keycloakProvider" => $inProduction ?
                KeycloakProvider::createForProduction() :
                KeycloakProvider::createForDevelopment(),
            "audienceId" => "authorization-service-api",
        ];
        $options = array_merge($defaultOptions, $options);

        $keycloak = $options["keycloakProvider"];
        $result = $keycloak->apiAccess(
            $clientId,
            $clientSecret,
            $options["audienceId"]
        );

        return new static($result->accessToken(), $options, $inProduction);
    }

    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    protected function verifiedAccessToken(): string
    {
        return $this->accessToken;
    }
}
