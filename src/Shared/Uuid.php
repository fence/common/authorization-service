<?php

namespace Glance\AuthorizationService\Shared;

class Uuid
{
    /** @var string */
    private $uuid;

    final private function __construct(string $uuid)
    {
        $this->uuid = $uuid;
    }

    /** @return static */
    public static function fromString(string $uuid): object
    {
        return new static($uuid);
    }

    public function toString(): string
    {
        return $this->uuid;
    }

    /**
     * @param string[] $uuids
     *
     * @return static[]
     */
    public static function fromStrings(array $uuids): array
    {
        return array_map(
            function (string $uuid): object {
                return static::fromString($uuid);
            },
            $uuids
        );
    }

    /**
     * @param static[] $ids
     *
     * @return string[]
     */
    public static function toStrings(array $ids): array
    {
        return array_map(
            function ($id): string {
                return $id->toString();
            },
            $ids
        );
    }

    /**
     * @param static[] $ids1
     * @param static[] $ids2
     *
     * @return static[]
     */
    public static function diff(array $ids1, array $ids2): array
    {
        $diff = array_diff(static::toStrings($ids1), static::toStrings($ids2));

        return static::fromStrings($diff);
    }
}
