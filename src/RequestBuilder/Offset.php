<?php

namespace Glance\AuthorizationService\RequestBuilder;

/**
 * Input to define results offset
 */
class Offset implements Input
{
    /** @var int */
    private $offset;

    private function __construct(int $offset)
    {
        $this->offset = $offset;
    }

    /**
     * Create from integer
     *
     * @param int $limit
     *
     * @return self
     */
    public static function fromInteger(int $offset): self
    {
        return new self($offset);
    }

    /** {@inheritDoc} */
    public function toQuery(): string
    {
        return "offset={$this->offset}";
    }
}
