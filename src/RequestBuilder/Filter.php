<?php

namespace Glance\AuthorizationService\RequestBuilder;

use InvalidArgumentException;

/**
 * Filter input
 *
 * @todo Filters also accept operator
 */
class Filter implements Input
{
    /** @var string */
    private $field;

    /** @var string */
    private $value;

    /** @var string */
    private $operator;

    private function __construct(string $field, string $value, string $operator)
    {
        $this->field = $field;
        $this->value = $value;

        $allowedOperators = [ "eq", "neq", "startswith", "gt", "lt", "contains" ];
        if (!in_array(strtolower($operator), $allowedOperators)) {
            throw new InvalidArgumentException("Invalid operator.");
        }
        $this->operator = $operator;
    }

    /**
     * Create from strings
     *
     * @param string $field
     * @param string $value
     * @param string $operator
     *
     * @return self
     */
    public static function fromStrings(string $field, string $value, string $operator): self
    {
        return new self($field, $value, $operator);
    }

    public static function equals(string $field, string $value): self
    {
        return new self($field, $value, "eq");
    }

    public static function notEquals(string $field, string $value): self
    {
        return new self($field, $value, "neq");
    }

    /** {@inheritDoc} */
    public function toQuery(): string
    {
        return "filter={$this->field}:{$this->operator}:{$this->value}";
    }

    public function getField(): string
    {
        return $this->field;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function getOperator(): string
    {
        return $this->operator;
    }
}
