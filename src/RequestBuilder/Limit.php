<?php

namespace Glance\AuthorizationService\RequestBuilder;

/**
 * Input to limit the number of results
 */
class Limit implements Input
{
    /** @var int */
    private $limit;
    public const MAXIMUM_LIMIT = 5000;

    private function __construct(int $limit)
    {
        $this->limit = $limit;
    }

    /**
     * Create from integer
     *
     * @param int $limit
     *
     * @return self
     */
    public static function fromInteger(int $limit): self
    {
        return new self($limit);
    }

    /** {@inheritDoc} */
    public function toQuery(): string
    {
        return "limit={$this->limit}";
    }
}
