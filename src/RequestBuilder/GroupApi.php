<?php

namespace Glance\AuthorizationService\RequestBuilder;

use Psr\Http\Message\ServerRequestFactoryInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriFactoryInterface;

class GroupApi
{
    /** @var ServerRequestFactoryInterface */
    private $serverRequestFactory;

    /** @var UriFactoryInterface */
    private $uriFactory;

    /** @var string */
    private $host;

    /** @var string */
    private $basePath;

    private function __construct(
        ServerRequestFactoryInterface $serverRequestFactory,
        UriFactoryInterface $uriFactory,
        string $host,
        string $basePath
    ) {
        $this->serverRequestFactory = $serverRequestFactory;
        $this->uriFactory = $uriFactory;
        $this->host = $host;
        $this->basePath = $basePath;
    }

    /**
     * Create
     *
     * @param ServerRequestFactoryInterface&UriFactoryInterface $psr17factory
     * @param string $host
     * @param string $basePath
     *
     * @return self
     */
    public static function createFromPsr17Factory(
        object $psr17factory,
        string $host = "authorization-service-api.web.cern.ch",
        string $basePath = "api/v1.0"
    ): self {
        return new self($psr17factory, $psr17factory, $host, $basePath);
    }

    /**
     * Request for get groups
     *
     * @param Input[] $inputs
     * @param string $accessToken
     *
     * @return ServerRequestInterface
     */
    public function getGroups(array $inputs, string $accessToken): ServerRequestInterface
    {
        $queries = array_map(
            function (Input $input) {
                return $input->toQuery();
            },
            $inputs
        );
        $query = implode("&", $queries);

        $uri = $this->uriFactory
            ->createUri()
            ->withScheme("https")
            ->withHost($this->host)
            ->withPath($this->basePath . "/Group")
            ->withQuery($query);

        return $this->serverRequestFactory
            ->createServerRequest("GET", $uri)
            ->withAddedHeader("Authorization", "Bearer {$accessToken}");
    }

    public function createGroup(array $groupData, string $accessToken): ServerRequestInterface
    {
        $uri = $this->uriFactory
            ->createUri()
            ->withScheme("https")
            ->withHost($this->host)
            ->withPath($this->basePath . "/Group");

        $request = $this->serverRequestFactory
            ->createServerRequest("POST", $uri)
            ->withAddedHeader("Authorization", "Bearer {$accessToken}")
            ->withAddedHeader("Content-Type", "application/json");

        $request->getBody()->write(json_encode($groupData));

        return $request;
    }

  /**
     * @param string $groupId
     * @param array{
     *   groupIdentifier: string,
     *   displayName: string,
     *   description: string,
     *   administratorsId?: ?string,
     *   resourceCategory: string,
     *   selfSubscriptionType: string,
     *   privacyType: string,
     *   approvalRequired: bool,
     *   syncType?: ?string,
     *   message?: string
     * } $groupData
     * @param string $accessToken
     * @return ServerRequestInterface
     */
    public function updateGroup(string $groupId, array $groupData, string $accessToken): ServerRequestInterface
    {
        $uri = $this->uriFactory
            ->createUri()
            ->withScheme("https")
            ->withHost($this->host)
            ->withPath($this->basePath . "/Group/{$groupId}");

        $request = $this->serverRequestFactory
            ->createServerRequest("PATCH", $uri)
            ->withAddedHeader("Authorization", "Bearer {$accessToken}")
            ->withAddedHeader("Content-Type", "application/json");

        $payload = [];
        /** @var string $key */
        /** @var mixed $value */
        foreach ($groupData as $key => $value) {
            $payload[] = [
                "path" => $key,
                "value" => $value,
                "op" => "replace",
                "from" => null
            ];
        }

        $request->getBody()->write(json_encode($payload));
        return $request;
    }

    public function deleteGroup(string $groupId, string $accessToken): ServerRequestInterface
    {
        $uri = $this->uriFactory
            ->createUri()
            ->withScheme("https")
            ->withHost($this->host)
            ->withPath($this->basePath . "/Group/{$groupId}");

        $request = $this->serverRequestFactory
            ->createServerRequest("DELETE", $uri)
            ->withAddedHeader("Authorization", "Bearer {$accessToken}")
            ->withAddedHeader("Content-Type", "application/json");

        return $request;
    }

    public function getGroupMembers(
        string $groupId,
        array $inputs,
        string $accessToken
    ): ServerRequestInterface {
        $queries = array_map(
            function (Input $input) {
                return $input->toQuery();
            },
            $inputs
        );
        $query = implode("&", $queries);

        $uri = $this->uriFactory
            ->createUri()
            ->withScheme("https")
            ->withHost($this->host)
            ->withPath($this->basePath . "/Group/{$groupId}/memberidentities")
            ->withQuery($query);

        return $this->serverRequestFactory
            ->createServerRequest("GET", $uri)
            ->withAddedHeader("Authorization", "Bearer {$accessToken}");
    }

    public function addMembersToGroup(
        string $groupId,
        array $identityIds,
        string $accessToken
    ): ServerRequestInterface {
        $queries = array_map(
            function ($id) {
                return "ids={$id}";
            },
            $identityIds
        );
        $query = implode("&", $queries);

        $uri = $this->uriFactory
            ->createUri()
            ->withScheme("https")
            ->withHost($this->host)
            ->withPath($this->basePath . "/Group/{$groupId}/memberidentities")
            ->withQuery($query);

        return $this->serverRequestFactory
            ->createServerRequest("POST", $uri)
            ->withAddedHeader("Authorization", "Bearer {$accessToken}");
    }

    public function getGroupMembersRecursive(
        string $groupId,
        array $inputs,
        string $accessToken
    ): ServerRequestInterface {
        $queries = array_map(
            function (Input $input) {
                return $input->toQuery();
            },
            $inputs
        );
        $query = implode("&", $queries);

        $uri = $this->uriFactory
            ->createUri()
            ->withScheme("https")
            ->withHost($this->host)
            ->withPath($this->basePath . "/Group/{$groupId}/memberidentities/precomputed")
            ->withQuery($query);

        return $this->serverRequestFactory
            ->createServerRequest("GET", $uri)
            ->withAddedHeader("Authorization", "Bearer {$accessToken}");
    }

    public function removeMembersFromGroup(
        string $groupId,
        array $identityIds,
        string $accessToken
    ): ServerRequestInterface {
        $queries = array_map(
            function ($id) {
                return "ids={$id}";
            },
            $identityIds
        );
        $query = implode("&", $queries);

        $uri = $this->uriFactory
            ->createUri()
            ->withScheme("https")
            ->withHost($this->host)
            ->withPath($this->basePath . "/Group/{$groupId}/memberidentities")
            ->withQuery($query);

        return $this->serverRequestFactory
            ->createServerRequest("DELETE", $uri)
            ->withAddedHeader("Authorization", "Bearer {$accessToken}");
    }

    public function getGroupMemberGroups(
        string $groupId,
        array $inputs,
        string $accessToken
    ): ServerRequestInterface {
        $queries = array_map(
            function (Input $input) {
                return $input->toQuery();
            },
            $inputs
        );
        $query = implode("&", $queries);

        $uri = $this->uriFactory
            ->createUri()
            ->withScheme("https")
            ->withHost($this->host)
            ->withPath($this->basePath . "/Group/{$groupId}/membergroups")
            ->withQuery($query);

        return $this->serverRequestFactory
            ->createServerRequest("GET", $uri)
            ->withAddedHeader("Authorization", "Bearer {$accessToken}");
    }

    public function removeMemberGroupsFromGroup(
        string $groupId,
        array $identityIds,
        string $accessToken
    ): ServerRequestInterface {
        $queries = array_map(
            function ($id) {
                return "ids={$id}";
            },
            $identityIds
        );
        $query = implode("&", $queries);

        $uri = $this->uriFactory
            ->createUri()
            ->withScheme("https")
            ->withHost($this->host)
            ->withPath($this->basePath . "/Group/{$groupId}/membergroups")
            ->withQuery($query);

        return $this->serverRequestFactory
            ->createServerRequest("DELETE", $uri)
            ->withAddedHeader("Authorization", "Bearer {$accessToken}");
    }

    public function addMemberGroupsToGroup(
        string $groupId,
        array $identityIds,
        string $accessToken
    ): ServerRequestInterface {
        $queries = array_map(
            function ($id) {
                return "ids={$id}";
            },
            $identityIds
        );
        $query = implode("&", $queries);

        $uri = $this->uriFactory
            ->createUri()
            ->withScheme("https")
            ->withHost($this->host)
            ->withPath($this->basePath . "/Group/{$groupId}/membergroups")
            ->withQuery($query);

        return $this->serverRequestFactory
            ->createServerRequest("POST", $uri)
            ->withAddedHeader("Authorization", "Bearer {$accessToken}");
    }

    public function transferGroupOwnership(
        string $groupId,
        string $newOwnerId,
        string $accessToken
    ): ServerRequestInterface {
        $uri = $this->uriFactory
            ->createUri()
            ->withScheme("https")
            ->withHost($this->host)
            ->withPath(
                $this->basePath . "/Group/{$groupId}/transfer/{$newOwnerId}"
            );

        return $this->serverRequestFactory
            ->createServerRequest("PUT", $uri)
            ->withAddedHeader("Authorization", "Bearer {$accessToken}");
    }
}
