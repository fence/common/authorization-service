<?php

namespace Glance\AuthorizationService\RequestBuilder;

use InvalidArgumentException;

/**
 * Input for sorting results
 */
class Sort implements Input
{
    /** @var string */
    private $field;

    /** @var string */
    private $direction;

    private function __construct(string $field, string $direction)
    {
        if ($direction !== "asc" && $direction !== "desc") {
            throw new InvalidArgumentException("Sort direction should be either 'asc' or 'desc'.");
        }

        $this->field = $field;
        $this->direction = $direction;
    }

    /**
     * Create from array
     *
     * @param string $field
     * @param string $direction
     *
     * @return self
     */
    public static function fromStrings(string $field, string $direction): self
    {
        return new self($field, $direction);
    }

    /** {@inheritDoc} */
    public function toQuery(): string
    {
        return "sort={$this->field}:{$this->direction}";
    }

    public function getField(): string
    {
        return $this->field;
    }

    public function getDirection(): string
    {
        return $this->direction;
    }
}
