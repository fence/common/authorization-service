<?php

namespace Glance\AuthorizationService\RequestBuilder;

use Psr\Http\Message\ServerRequestFactoryInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriFactoryInterface;

class IdentityApi
{
    /** @var ServerRequestFactoryInterface */
    private $serverRequestFactory;

    /** @var UriFactoryInterface */
    private $uriFactory;

    /** @var string */
    private $host;

    /** @var string */
    private $basePath;

    private function __construct(
        ServerRequestFactoryInterface $serverRequestFactory,
        UriFactoryInterface $uriFactory,
        string $host,
        string $basePath
    ) {
        $this->serverRequestFactory = $serverRequestFactory;
        $this->uriFactory = $uriFactory;
        $this->host = $host;
        $this->basePath = $basePath;
    }

    /**
     * Create
     *
     * @param ServerRequestFactoryInterface&UriFactoryInterface $psr17factory
     * @param string $host
     * @param string $basePath
     *
     * @return self
     */
    public static function createFromPsr17Factory(
        object $psr17factory,
        string $host = "authorization-service-api.web.cern.ch",
        string $basePath = "api/v1.0"
    ): self {
        return new self($psr17factory, $psr17factory, $host, $basePath);
    }

    /**
     * Request for get identities
     *
     * @param Input[] $inputs
     * @param string $accessToken
     *
     * @return ServerRequestInterface
     */
    public function getIdentities(array $inputs, string $accessToken): ServerRequestInterface
    {
        $queries = array_map(
            function (Input $input) {
                return $input->toQuery();
            },
            $inputs
        );
        $query = implode("&", $queries);

        $uri = $this->uriFactory
            ->createUri()
            ->withScheme("https")
            ->withHost($this->host)
            ->withPath($this->basePath . "/Identity")
            ->withQuery($query);

        return $this->serverRequestFactory
            ->createServerRequest("GET", $uri)
            ->withAddedHeader("Authorization", "Bearer {$accessToken}");
    }
}
