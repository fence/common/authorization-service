<?php

namespace Glance\AuthorizationService\RequestBuilder;

/**
 * Field input
 *
 * Used to control which resource fields are returned on the response body.
 */
class Field implements Input
{
    /** @var string */
    private $field;

    private function __construct(string $field)
    {
        $this->field = $field;
    }

    /**
     * Create from string
     *
     * @param string $field
     *
     * @return self
     */
    public static function fromString(string $field): self
    {
        return new self($field);
    }

    /** {@inheritDoc} */
    public function toQuery(): string
    {
        return "field={$this->field}";
    }

    public function getField(): string
    {
        return $this->field;
    }
}
