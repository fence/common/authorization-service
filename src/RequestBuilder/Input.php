<?php

namespace Glance\AuthorizationService\RequestBuilder;

interface Input
{
    /**
     * Convert input to URI query string
     *
     * @return string
     */
    public function toQuery(): string;
}
