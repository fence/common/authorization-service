<?php

namespace Glance\AuthorizationService\Tests\Unit\Shared;

use Glance\AuthorizationService\Shared\Uuid;
use PHPUnit\Framework\TestCase;

final class UuidTest extends TestCase
{
    public function testFromString(): void
    {
        $value = "2c2cd396-67cb-491b-95b1-9dc7f1b6602e";
        $uuid = Uuid::fromString($value);

        $this->assertEquals($value, $uuid->toString());
    }
}
