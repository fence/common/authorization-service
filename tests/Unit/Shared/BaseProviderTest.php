<?php

namespace Glance\AuthorizationService\Tests\Unit\Shared;

use Glance\AuthorizationService\Shared\BaseProvider;
use Glance\CernAuthentication\KeycloakProvider;
use Glance\CernAuthentication\Result;
use PHPUnit\Framework\TestCase;

final class BaseProviderTest extends TestCase
{
    public function testCreateWithAccessToken(): void
    {
        $accessToken = $this->accessToken();
        $provider = BaseProvider::createWithAccessToken($accessToken);

        $this->assertEquals($accessToken, $provider->getAccessToken());
    }

    public function testCreateWithAppCredentials(): void
    {
        $accessToken = $this->accessToken();

        $keycloakProvider = $this->createStub(KeycloakProvider::class);
        $keycloakProvider->method("apiAccess")
                         ->willReturn(Result::fromArray([ "access_token" => $accessToken ]));

        $provider = BaseProvider::createWithAppCredentials(
            "client-id",
            "client-secret",
            [ "keycloakProvider" => $keycloakProvider ]
        );

        $this->assertEquals($accessToken, $provider->getAccessToken());
    }

    private function accessToken(): string
    {
        //phpcs:ignore Generic.Files.LineLength.TooLong
        return "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiTWFyaW8ifQ.0_1g4Zit9MJi0-Xc1XYaNEvm1TY9lACgQPq-DUGXMsY";
    }
}
