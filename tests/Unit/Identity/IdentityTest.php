<?php

namespace Glance\AuthorizationService\Tests\Unit\Identity;

use Glance\AuthorizationService\Identity\Identity;
use PHPUnit\Framework\TestCase;

final class IdentityTest extends TestCase
{
    public function testFromArray(): void
    {
        $array = $this->validIdentityArray();

        $identity = Identity::fromArray($array);

        $this->assertSame($array, $identity->toArray());
        $this->assertSame($array["id"], $identity->id()->toString());
        $this->assertSame($array["type"], $identity->type()->toString());
        $this->assertSame($array["upn"], $identity->upn());
        $this->assertSame($array["displayName"], $identity->displayName());
        $this->assertSame((int) $array["personId"], $identity->personId()->toInteger());
        $this->assertSame($array["lastName"], $identity->lastName());
        $this->assertSame($array["firstName"], $identity->firstName());
    }

    private function validIdentityArray(): array
    {
        return [
            "id"                 => "59edf297-bd07-4000-904d-80cc6288004a",
            "type"               => "Person",
            "upn"                => "mgunters",
            "displayName"        => "Mario Gunter Simao",
            "personId"           => "837034",
            "lastName"           => "Gunter Simao",
            "firstName"          => "Mario",
            "disabled"           => false,
            "blockingReason"     => null,
        ];
    }
}
