<?php

namespace Glance\AuthorizationService\Tests\Unit\Identity;

use Glance\AuthorizationService\Identity\IdentityType;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class IdentityTypeTest extends TestCase
{
    public function testFromString(): void
    {
        $typeString = "Person";
        $category = IdentityType::fromString($typeString);

        $this->assertEquals($typeString, $category->toString());
    }

    public function testFromInvalidString(): void
    {
        $invalidType = "Potato";

        $this->expectException(InvalidArgumentException::class);
        IdentityType::fromString($invalidType);
    }

    public function testPerson(): void
    {
        $category = IdentityType::person();

        $this->assertEquals("Person", $category->toString());
    }

    public function testService(): void
    {
        $category = IdentityType::service();

        $this->assertEquals("Service", $category->toString());
    }

    public function testSecondary(): void
    {
        $category = IdentityType::secondary();

        $this->assertEquals("Secondary", $category->toString());
    }
}
