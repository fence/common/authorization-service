<?php

namespace Glance\AuthorizationService\Tests\Unit\Identity;

use Glance\AuthorizationService\Identity\Exception\FailedToFindAllIdentitiesException;
use Glance\AuthorizationService\Identity\Exception\FailedToFindIdentityByPersonIdException;
use Glance\AuthorizationService\Identity\Exception\FailedToFindIdentityByUpnException;
use Glance\AuthorizationService\Identity\Exception\FailedToFindIdentityIdException;
use Glance\AuthorizationService\Identity\Exception\FailedToSearchIdentityException;
use Glance\AuthorizationService\Identity\Exception\FailedToFindPersonByIdentityIdException;
use Glance\AuthorizationService\Identity\Exception\PersonIdNotFoundException;
use Glance\AuthorizationService\Identity\Exception\UpnNotFoundException;
use Glance\AuthorizationService\Identity\Exception\IdentityIdNotFoundException;
use Glance\AuthorizationService\Identity\IdentityId;
use Glance\AuthorizationService\Identity\IdentityProvider;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware as GuzzleMiddleware;
use Nyholm\Psr7\Factory\Psr17Factory;
use Nyholm\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

final class IdentityProviderTest extends TestCase
{
    public function testGetIdsFromPersonIdsWithGuzzle(): void
    {
        $mock = new MockHandler([
            new Response(200, [], '{ "data": [{ "id": "59edf297-bd07-4000-904d-80cc6288004a" }] }'),
            new Response(200, [], '{ "data": [{ "id": "dbcd6647-fd50-493e-9162-635f355794d4" }] }'),
            new Response(200, [], '{ "data": [{ "id": "f41f0213-901b-4fbb-b542-9d32578eaa5f" }] }'),
        ]);

        $handlerStack = HandlerStack::create($mock);
        $client = new Client(['handler' => $handlerStack]);

        $identityProvider = IdentityProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $ids = $identityProvider->getIdsFromPersonIds([837034, 823204, 755572]);

        $this->assertEquals(
            [
                IdentityId::fromString("59edf297-bd07-4000-904d-80cc6288004a"),
                IdentityId::fromString("dbcd6647-fd50-493e-9162-635f355794d4"),
                IdentityId::fromString("f41f0213-901b-4fbb-b542-9d32578eaa5f"),
            ],
            $ids
        );
    }

    public function testGetIdsFromPersonIdsFailDueToStatusGreaterThan400(): void
    {
        $mock = new MockHandler([
            new Response(200, [], '{ "data": [{ "id": "59edf297-bd07-4000-904d-80cc6288004a" }] }'),
            new Response(400, [], '{ "message": "Error message." }'),
            new Response(200, [], '{ "data": [{ "id": "f41f0213-901b-4fbb-b542-9d32578eaa5f" }] }'),
        ]);

        $handlerStack = HandlerStack::create($mock);
        $client = new Client(['handler' => $handlerStack]);

        $identityProvider = IdentityProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $this->expectException(FailedToFindIdentityIdException::class);
        $identityProvider->getIdsFromPersonIds([837034, 823204, 755572]);
    }

    public function testGetIdsFromPersonIdsFailDueToEmptyResponseBodyData(): void
    {
        $mock = new MockHandler([
            new Response(200, [], '{ "data": [{ "id": "59edf297-bd07-4000-904d-80cc6288004a" }] }'),
            new Response(200, [], '{ "data": []'),
            new Response(200, [], '{ "data": [{ "id": "f41f0213-901b-4fbb-b542-9d32578eaa5f" }] }'),
        ]);

        $handlerStack = HandlerStack::create($mock);
        $client = new Client(['handler' => $handlerStack]);

        $identityProvider = IdentityProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $this->expectException(FailedToFindIdentityIdException::class);
        $identityProvider->getIdsFromPersonIds([837034, 381497, 755572]);
    }

    public function testFindIdsFromPersonIdsWithGuzzle(): void
    {
        $mock = new MockHandler([
            new Response(200, [], '{ "data": [{ "id": "59edf297-bd07-4000-904d-80cc6288004a" }] }'),
            new Response(400, [], '{ "message": "Error message." }'),
            new Response(200, [], '{ "data": [{ "id": "f41f0213-901b-4fbb-b542-9d32578eaa5f" }] }'),
        ]);

        $handlerStack = HandlerStack::create($mock);
        $client = new Client(['handler' => $handlerStack]);

        $identityProvider = IdentityProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $ids = $identityProvider->findIdsFromPersonIds([837034, 823204, 755572]);

        $this->assertEquals(
            [
                IdentityId::fromString("59edf297-bd07-4000-904d-80cc6288004a"),
                IdentityId::fromString("f41f0213-901b-4fbb-b542-9d32578eaa5f"),
            ],
            $ids
        );
    }

    public function testFindIdsFromPersonIds(): void
    {
        $client = new \Http\Mock\Client();
        $fakeResponse1 = $this->fakeResponseWithBody([ "data" => [[ "id" => "59edf297-bd07-4000-904d-80cc6288004a"]] ]);
        $fakeResponse2 = $this->fakeResponseWithBody([ "data" => [] ]);
        $fakeResponse3 = $this->fakeResponseWithBody([ "data" => [[ "id" => "f41f0213-901b-4fbb-b542-9d32578eaa5f"]] ]);
        $client->addResponse($fakeResponse1);
        $client->addResponse($fakeResponse2);
        $client->addResponse($fakeResponse3);

        $identityProvider = IdentityProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $ids = $identityProvider->findIdsFromPersonIds([837034, 823204, 755572]);

        $this->assertEquals(
            [
                IdentityId::fromString("59edf297-bd07-4000-904d-80cc6288004a"),
                IdentityId::fromString("f41f0213-901b-4fbb-b542-9d32578eaa5f"),
            ],
            $ids
        );
    }

    public function testFindIdsFromManyPersonIds(): void
    {
        $client = new \Http\Mock\Client();
        $fakeResponses = [
            $this->fakeResponseWithBody([ "data" => [
            [ "id" => "59edf297-bd07-4000-904d-80cc6288004a", "personId" => "837034"],
            [ "id" => "f41f0213-901b-4fbb-b542-9d32578eaa5f", "personId" => "755572"]
            ] ]),
            $this->fakeResponseWithBody([ "data" => [] ]),
            $this->fakeResponseWithBody([ "data" => [] ]),
            $this->fakeResponseWithBody([ "data" => [] ]),
            $this->fakeResponseWithBody([ "data" => [] ]),
            $this->fakeResponseWithBody([ "data" => [] ]),
            $this->fakeResponseWithBody([ "data" => [] ]),
            $this->fakeResponseWithBody([ "data" => [] ]),
            $this->fakeResponseWithBody([ "data" => [] ]),
            $this->fakeResponseWithBody([ "data" => [] ]),
            $this->fakeResponseWithBody([ "data" => [] ]),
            $this->fakeResponseWithBody([ "data" => [] ]),
            $this->fakeResponseWithBody([ "data" => [] ])
        ];
        foreach ($fakeResponses as $fakeResponse) {
            $client->addResponse($fakeResponse);
        }

        $identityProvider = IdentityProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $personIds = [837034, 823204, 755572];
        $ids = $identityProvider->findIdsFromManyPersonIds($personIds);

        $this->assertEquals(
            [
                IdentityId::fromString("59edf297-bd07-4000-904d-80cc6288004a"),
                IdentityId::fromString("f41f0213-901b-4fbb-b542-9d32578eaa5f"),
                null,
            ],
            [
                $ids[837034],
                $ids[755572],
                $ids[823204]
            ]
        );
    }

    public function testFindIdsFromManyPersonIdsFails(): void
    {
        $client = new \Http\Mock\Client();
        $fakeResponse = new Response(500, [], '{ "message": "Error message." }');
        $client->addResponse($fakeResponse);

        $identityProvider = IdentityProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $personIds = [837034, 823204, 755572];
        $this->expectException(FailedToFindAllIdentitiesException::class);

        $identityProvider->findIdsFromManyPersonIds($personIds);
    }

    public function testSearchByName(): void
    {
        $identitiesData = $this->validIdentitiesArray();
        array_shift($identitiesData);
        $lookupText = "d";

        $client = new \Http\Mock\Client();
        $fakeResponse = $this->fakeResponseWithBody([ "data" => $identitiesData ]);
        $client->addResponse($fakeResponse);

        $identityProvider = IdentityProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $identities = $identityProvider->searchByName($lookupText);
        foreach ($identities as $identity) {
            $this->assertEquals(
                true,
                strpos(strtoupper($identity->displayName()), strtoupper($lookupText))
            );
        }
    }

    public function testSearchByNameFail(): void
    {
        $identitiesData = $this->validIdentitiesArray();
        array_shift($identitiesData);
        $lookupText = "d";

        $client = new \Http\Mock\Client();
        $client->addResponse(new Response(500, [], '{ "message": "Error message." }'));

        $identityProvider = IdentityProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $this->expectException(FailedToSearchIdentityException::class);
        $identityProvider->searchByName($lookupText);
    }

    public function testFindByUpn(): void
    {
        $identityData = $this->validIdentityData();

        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $identityData ],
                ])
            ),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = IdentityProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $identity = $groupProvider->findByUpn("mgunters");

        $this->assertEquals($identityData, $identity->toArray());
        $this->assertEquals(1, count($history));
    }

    public function testFindByUpnFail(): void
    {
        $mock = new MockHandler([ new Response(500, [], '{ "message": "Error message" }') ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $identityProvider = IdentityProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $this->expectException(FailedToFindIdentityByUpnException::class);
        $identityProvider->findByUpn("mgunters");
    }

    public function testFindByUpnWhenUpnNotFound(): void
    {
        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 0 ],
                    "data" => [],
                ])
            ),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $identityProvider = IdentityProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $this->expectException(UpnNotFoundException::class);
        $identityProvider->findByUpn("mgunters");
    }

    public function testFindByPersonId(): void
    {
        $identityData = $this->validIdentityData();

        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $identityData ],
                ])
            ),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = IdentityProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $identity = $groupProvider->findByPersonId(840720);

        $this->assertEquals($identityData, $identity->toArray());
        $this->assertEquals(1, count($history));
    }

    public function testFindByPersonIdFail(): void
    {
        $mock = new MockHandler([ new Response(500, [], '{ "message": "Error message" }') ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $identityProvider = IdentityProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $this->expectException(FailedToFindIdentityByPersonIdException::class);
        $identityProvider->findByPersonId(840720);
    }

    public function testFindByPersonIdWhenPersonIdNotFound(): void
    {
        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 0 ],
                    "data" => [],
                ])
            ),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $identityProvider = IdentityProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $this->expectException(PersonIdNotFoundException::class);
        $identityProvider->findByPersonId(840720);
    }

    public function testFindByIdentityId(): void
    {
        $identityData = $this->validIdentityData();

        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $identityData ],
                ])
            ),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = IdentityProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $identity = $groupProvider->findByIdentityId($this->validIdentityData()["id"]);

        $this->assertEquals($identityData, $identity->toArray());
        $this->assertEquals(1, count($history));
    }

    public function testFindByIdentityIdFail(): void
    {
        $mock = new MockHandler([ new Response(500, [], '{ "message": "Error message" }') ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $identityProvider = IdentityProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $this->expectException(FailedToFindPersonByIdentityIdException::class);
        $identityProvider->findByIdentityId($this->validIdentityData()["id"]);
    }

    public function testFindByIdentityIdFailWhenIdentityIdNotFound(): void
    {
        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 0 ],
                    "data" => [],
                ])
            ),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $identityProvider = IdentityProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $this->expectException(IdentityIdNotFoundException::class);
        $identityProvider->findByIdentityId($this->validIdentityData()["id"]);
    }

    public function testFindUnblockedIdsFromManyPersonIds(): void
    {
        $client = new \Http\Mock\Client();
        $fakeResponses = [
            $this->fakeResponseWithBody([ "data" => [
            [ "id" => "59edf297-bd07-4000-904d-80cc6288004a", "personId" => "837034", "disabled" => "false"],
            [ "id" => "f41f0213-901b-4fbb-b542-9d32578eaa5f", "personId" => "755572", "disabled" => "true"],
            [ "id" => "f41f0213-901b-4fbb-b542-9d32578eaa5g", "personId" => "755572", "disabled" => "false"]
            ] ]),
            $this->fakeResponseWithBody([ "data" => [] ]),
            $this->fakeResponseWithBody([ "data" => [] ]),
            $this->fakeResponseWithBody([ "data" => [] ]),
            $this->fakeResponseWithBody([ "data" => [] ]),
            $this->fakeResponseWithBody([ "data" => [] ]),
            $this->fakeResponseWithBody([ "data" => [] ]),
            $this->fakeResponseWithBody([ "data" => [] ]),
            $this->fakeResponseWithBody([ "data" => [] ]),
            $this->fakeResponseWithBody([ "data" => [] ]),
            $this->fakeResponseWithBody([ "data" => [] ]),
            $this->fakeResponseWithBody([ "data" => [] ]),
            $this->fakeResponseWithBody([ "data" => [] ])
        ];
        foreach ($fakeResponses as $fakeResponse) {
            $client->addResponse($fakeResponse);
        }

        $identityProvider = IdentityProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $personIds = [837034, 823204, 755572];
        $ids = $identityProvider->findUnblockedIdsFromManyPersonIds($personIds);

        $this->assertEquals(
            [
                IdentityId::fromString("59edf297-bd07-4000-904d-80cc6288004a"),
                IdentityId::fromString("f41f0213-901b-4fbb-b542-9d32578eaa5g"),
                null,
            ],
            [
                $ids[837034],
                $ids[755572],
                $ids[823204]
            ]
        );
    }

    public function testFindUnblockedIdsFromManyPersonIdsFails(): void
    {
        $client = new \Http\Mock\Client();
        $fakeResponse = new Response(500, [], '{ "message": "Error message." }');
        $client->addResponse($fakeResponse);

        $identityProvider = IdentityProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $personIds = [837034, 823204, 755572];
        $this->expectException(FailedToFindAllIdentitiesException::class);

        $identityProvider->findIdsFromManyPersonIds($personIds);
    }

    private function validIdentityData(): array
    {
        return [
            "id"                 => "59edf297-bd07-4000-904d-80cc6288004a",
            "type"               => "Person",
            "upn"                => "mgunters",
            "displayName"        => "Mario Gunter Simao",
            "personId"           => "837034",
            "lastName"           => "Gunter Simao",
            "firstName"          => "Mario",
            "disabled"           => false,
            "blockingReason"     => null,
        ];
    }

    private function validIdentitiesArray(): array
    {
        return [
            [
                "id"                 => "59edf297-bd07-4000-904d-80cc6288004a",
                "type"               => "Person",
                "upn"                => "mgunters",
                "displayName"        => "Mario Gunter Simao",
                "personId"           => "837034",
                "lastName"           => "Gunter Simao",
                "firstName"          => "Mario",
                "disabled"           => false,
                "blockingReason"     => null,
            ],
            [
                "id"                 => "dbcd6647-fd50-493e-9162-635f355794d4",
                "type"               => "Person",
                "upn"                => "midejesu",
                "displayName"        => "Michelly De Jesus Teixeira",
                "personId"           => "823204",
                "lastName"           => "De Jesus Teixeira",
                "firstName"          => "Michelly",
                "disabled"           => false,
                "blockingReason"     => null,
            ],
            [
                "id"                 => "f41f0213-901b-4fbb-b542-9d32578eaa5f",
                "type"               => "Person",
                "upn"                => "ldomingu",
                "displayName"        => "Leandro Domingues Macedo Alves",
                "personId"           => "755572",
                "lastName"           => "Domingues Macedo Alves",
                "firstName"          => "Leandro",
                "disabled"           => false,
                "blockingReason"     => null,
            ],
        ];
    }

    private function accessToken(): string
    {
        // phpcs:ignore Generic.Files.LineLength.TooLong
        return "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiTWFyaW8ifQ.0_1g4Zit9MJi0-Xc1XYaNEvm1TY9lACgQPq-DUGXMsY";
    }

    private function fakeResponseWithBody(array $body): ResponseInterface
    {
        $factory = new Psr17Factory();
        $fakeBody = $factory->createStream(json_encode($body));

        return $factory->createResponse()->withBody($fakeBody);
    }
}
