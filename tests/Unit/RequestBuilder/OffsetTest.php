<?php

namespace Glance\AuthorizationService\Tests\Unit\RequestBuilder;

use Glance\AuthorizationService\RequestBuilder\Offset;
use PHPUnit\Framework\TestCase;

final class OffsetTest extends TestCase
{
    public function testFromInteger(): void
    {
        $offset = Offset::fromInteger(1);

        $this->assertEquals("offset=1", $offset->toQuery());
    }
}
