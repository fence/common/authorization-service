<?php

namespace Glance\AuthorizationService\Tests\Unit\RequestBuilder;

use Glance\AuthorizationService\RequestBuilder\Field;
use PHPUnit\Framework\TestCase;

final class FieldTest extends TestCase
{
    public function testFromString(): void
    {
        $field = Field::fromString("personId");

        $this->assertEquals("personId", $field->getField());
    }

    public function testToQuery(): void
    {
        $field = Field::fromString("personId");

        $this->assertEquals("field=personId", $field->toQuery());
    }
}
