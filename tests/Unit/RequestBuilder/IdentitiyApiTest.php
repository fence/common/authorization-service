<?php

namespace Glance\AuthorizationService\Tests\Unit\RequestBuilder;

use Glance\AuthorizationService\RequestBuilder\Field;
use Glance\AuthorizationService\RequestBuilder\Filter;
use Glance\AuthorizationService\RequestBuilder\IdentityApi;
use Glance\AuthorizationService\RequestBuilder\Limit;
use Glance\AuthorizationService\RequestBuilder\Sort;
use Nyholm\Psr7\Factory\Psr17Factory;
use PHPUnit\Framework\TestCase;

final class IdentityApiTest extends TestCase
{
    public function testGetGroups(): void
    {
        $inputs = [
            Filter::equals("personId", "837034"),
            Field::fromString("firstName"),
            Sort::fromStrings("creationDate", "asc"),
            Limit::fromInteger(3),
        ];
        $accessToken = "dummy-access-token";

        $psr17Factory = new Psr17Factory();
        $groupApi = IdentityApi::createFromPsr17Factory($psr17Factory);
        $request = $groupApi->getIdentities($inputs, $accessToken);

        //phpcs:ignore Generic.Files.LineLength.TooLong
        $expectedUri = "https://authorization-service-api.web.cern.ch/api/v1.0/Identity?filter=personId:eq:837034&field=firstName&sort=creationDate:asc&limit=3";
        $this->assertEquals($expectedUri, (string) $request->getUri());
        $this->assertEquals("GET", $request->getMethod());
        $this->assertEquals([ "Bearer {$accessToken}" ], $request->getHeader("Authorization"));
    }
}
