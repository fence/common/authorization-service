<?php

namespace Glance\AuthorizationService\Tests\Unit\RequestBuilder;

use Glance\AuthorizationService\RequestBuilder\Sort;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class SortTest extends TestCase
{
    public function testFromStrings(): void
    {
        $sorts = Sort::fromStrings("personId", "asc");

        $this->assertEquals("personId", $sorts->getField());
        $this->assertEquals("asc", $sorts->getDirection());
    }

    public function testInvalidDirection(): void
    {
        $this->expectException(InvalidArgumentException::class);
        Sort::fromStrings("personId", "invalidDirection");
    }

    public function testToQuery(): void
    {
        $sorts = Sort::fromStrings("personId", "desc");

        $this->assertEquals("sort=personId:desc", $sorts->toQuery());
    }
}
