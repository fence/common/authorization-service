<?php

namespace Glance\AuthorizationService\Tests\Unit\RequestBuilder;

use Glance\AuthorizationService\RequestBuilder\Filter;
use PHPUnit\Framework\TestCase;

final class FilterTest extends TestCase
{
    public function testFromStrings(): void
    {
        $filter = Filter::fromStrings("personId", "837034", "eq");

        $this->assertEquals("personId", $filter->getField());
        $this->assertEquals("837034", $filter->getValue());
        $this->assertEquals("eq", $filter->getOperator());
    }

    public function testFromStringsWithInvalidOperator(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        Filter::fromStrings("personId", "837034", "foo");
    }

    public function testEquals(): void
    {
        $filter = Filter::equals("personId", "837034");

        $this->assertEquals("personId", $filter->getField());
        $this->assertEquals("837034", $filter->getValue());
        $this->assertEquals("eq", $filter->getOperator());
    }

    public function testToQuery(): void
    {
        $fields = Filter::fromStrings("personId", "837034", "eq");

        $this->assertEquals("filter=personId:eq:837034", $fields->toQuery());
    }
}
