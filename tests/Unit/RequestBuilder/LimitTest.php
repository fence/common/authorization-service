<?php

namespace Glance\AuthorizationService\Tests\Unit\RequestBuilder;

use Glance\AuthorizationService\RequestBuilder\Limit;
use PHPUnit\Framework\TestCase;

final class LimitTest extends TestCase
{
    public function testFromInteger(): void
    {
        $limit = Limit::fromInteger(1);

        $this->assertEquals("limit=1", $limit->toQuery());
    }
}
