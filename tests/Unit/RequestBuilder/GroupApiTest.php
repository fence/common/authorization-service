<?php

namespace Glance\AuthorizationService\Tests\Unit\RequestBuilder;

use Glance\AuthorizationService\RequestBuilder\Field;
use Glance\AuthorizationService\RequestBuilder\Filter;
use Glance\AuthorizationService\RequestBuilder\GroupApi;
use Glance\AuthorizationService\RequestBuilder\Limit;
use Glance\AuthorizationService\RequestBuilder\Sort;
use Nyholm\Psr7\Factory\Psr17Factory;
use PHPUnit\Framework\TestCase;

final class GroupApiTest extends TestCase
{
    public function testGetGroups(): void
    {
        $inputs = [
            Filter::equals("groupIdentifier", "group-name"),
            Filter::equals("ownerId", "f41f0213-901b-4fbb-b542-9d32578eaa5f"),
            Sort::fromStrings("creationDate", "asc"),
            Limit::fromInteger(3),
        ];
        $accessToken = "dummy-access-token";

        $psr17Factory = new Psr17Factory();
        $groupApi = GroupApi::createFromPsr17Factory($psr17Factory);
        $request = $groupApi->getGroups($inputs, $accessToken);

        //phpcs:ignore Generic.Files.LineLength.TooLong
        $expectedUri = "https://authorization-service-api.web.cern.ch/api/v1.0/Group?filter=groupIdentifier:eq:group-name&filter=ownerId:eq:f41f0213-901b-4fbb-b542-9d32578eaa5f&sort=creationDate:asc&limit=3";
        $this->assertEquals($expectedUri, (string) $request->getUri());
        $this->assertEquals("GET", $request->getMethod());
        $this->assertEquals([ "Bearer {$accessToken}" ], $request->getHeader("Authorization"));
    }

    public function testCreateGroup(): void
    {
        $groupData = [
            "groupIdentifier" => "test-group",
            "displayName"     => "Test Group",
            "description"     => "Another test group",
            "syncType"        => "Master",
        ];
        $accessToken = "dummy-access-token";

        $psr17Factory = new Psr17Factory();
        $groupApi = GroupApi::createFromPsr17Factory($psr17Factory);
        $request = $groupApi->createGroup($groupData, $accessToken);

        //phpcs:ignore Generic.Files.LineLength.TooLong
        $expectedUri = "https://authorization-service-api.web.cern.ch/api/v1.0/Group";
        $this->assertEquals($expectedUri, (string) $request->getUri());
        $this->assertEquals("POST", $request->getMethod());
        $this->assertEquals([ "Bearer {$accessToken}" ], $request->getHeader("Authorization"));
        $this->assertEquals(json_encode($groupData), (string) $request->getBody());
    }

    public function testDeleteGroup(): void
    {
        $groupId = "f41f0213-901b-4fbb-b542-9d32578eaa5f";
        $accessToken = "dummy-access-token";

        $psr17Factory = new Psr17Factory();
        $groupApi = GroupApi::createFromPsr17Factory($psr17Factory);
        $request = $groupApi->deleteGroup($groupId, $accessToken);

        //phpcs:ignore Generic.Files.LineLength.TooLong
        $expectedUri = "https://authorization-service-api.web.cern.ch/api/v1.0/Group/{$groupId}";
        $this->assertEquals($expectedUri, (string) $request->getUri());
        $this->assertEquals("DELETE", $request->getMethod());
        $this->assertEquals([ "Bearer {$accessToken}" ], $request->getHeader("Authorization"));
    }

    public function testGetGroupMembers(): void
    {
        $groupId = "f41f0213-901b-4fbb-b542-9d32578eaa5f";
        $accessToken = "dummy-access-token";

        $psr17Factory = new Psr17Factory();
        $groupApi = GroupApi::createFromPsr17Factory($psr17Factory);
        $request = $groupApi->getGroupMembers(
            $groupId,
            [
                Field::fromString("personId")
            ],
            $accessToken
        );

        //phpcs:ignore Generic.Files.LineLength.TooLong
        $expectedUri = "https://authorization-service-api.web.cern.ch/api/v1.0/Group/{$groupId}/memberidentities?field=personId";
        $this->assertEquals($expectedUri, (string) $request->getUri());
        $this->assertEquals("GET", $request->getMethod());
        $this->assertEquals([ "Bearer {$accessToken}" ], $request->getHeader("Authorization"));
    }

    public function testAddMembersToGroup(): void
    {
        $groupId = "f41f0213-901b-4fbb-b542-9d32578eaa5f";
        $members = [
            "0c3e909a-a224-427f-a2c0-067975e37de1",
            "b08ef523-04f5-4be6-9454-23ca250f3f57",
        ];
        $accessToken = "dummy-access-token";

        $psr17Factory = new Psr17Factory();
        $groupApi = GroupApi::createFromPsr17Factory($psr17Factory);
        $request = $groupApi->addMembersToGroup($groupId, $members, $accessToken);

        //phpcs:ignore Generic.Files.LineLength.TooLong
        $expectedUri = "https://authorization-service-api.web.cern.ch/api/v1.0/Group/{$groupId}/memberidentities?ids=0c3e909a-a224-427f-a2c0-067975e37de1&ids=b08ef523-04f5-4be6-9454-23ca250f3f57";
        $this->assertEquals($expectedUri, (string) $request->getUri());
        $this->assertEquals("POST", $request->getMethod());
        $this->assertEquals([ "Bearer {$accessToken}" ], $request->getHeader("Authorization"));
    }

    public function testRemoveMembersFromGroup(): void
    {
        $groupId = "f41f0213-901b-4fbb-b542-9d32578eaa5f";
        $members = [
            "0c3e909a-a224-427f-a2c0-067975e37de1",
            "b08ef523-04f5-4be6-9454-23ca250f3f57",
        ];
        $accessToken = "dummy-access-token";

        $psr17Factory = new Psr17Factory();
        $groupApi = GroupApi::createFromPsr17Factory($psr17Factory);
        $request = $groupApi->removeMembersFromGroup($groupId, $members, $accessToken);

        //phpcs:ignore Generic.Files.LineLength.TooLong
        $expectedUri = "https://authorization-service-api.web.cern.ch/api/v1.0/Group/{$groupId}/memberidentities?ids=0c3e909a-a224-427f-a2c0-067975e37de1&ids=b08ef523-04f5-4be6-9454-23ca250f3f57";
        $this->assertEquals($expectedUri, (string) $request->getUri());
        $this->assertEquals("DELETE", $request->getMethod());
        $this->assertEquals([ "Bearer {$accessToken}" ], $request->getHeader("Authorization"));
    }

    public function testGetGroupMemberGroups(): void
    {
        $groupId = "f41f0213-901b-4fbb-b542-9d32578eaa5f";
        $accessToken = "dummy-access-token";

        $psr17Factory = new Psr17Factory();
        $groupApi = GroupApi::createFromPsr17Factory($psr17Factory);
        $request = $groupApi->getGroupMemberGroups(
            $groupId,
            [
                Field::fromString("groupId")
            ],
            $accessToken
        );

        //phpcs:ignore Generic.Files.LineLength.TooLong
        $expectedUri = "https://authorization-service-api.web.cern.ch/api/v1.0/Group/{$groupId}/membergroups?field=groupId";
        $this->assertEquals($expectedUri, (string) $request->getUri());
        $this->assertEquals("GET", $request->getMethod());
        $this->assertEquals([ "Bearer {$accessToken}" ], $request->getHeader("Authorization"));
    }

    public function testRemoveGroupMemberGroups(): void
    {
        $groupId = "f41f0213-901b-4fbb-b542-9d32578eaa5f";
        $accessToken = "dummy-access-token";
        $identitiesRemoved = [
            "59edf297-bd07-4000-904d-80cc6288004a",
            "dbcd6647-fd50-493e-9162-635f355794d4",
            "f41f0213-901b-4fbb-b542-9d32578eaa5f",
        ];

        $psr17Factory = new Psr17Factory();
        $groupApi = GroupApi::createFromPsr17Factory($psr17Factory);
        $request = $groupApi->removeMembersFromGroup($groupId, $identitiesRemoved, $accessToken);

        //phpcs:ignore Generic.Files.LineLength.TooLong
        $expectedUri = "https://authorization-service-api.web.cern.ch/api/v1.0/Group/{$groupId}/memberidentities?ids={$identitiesRemoved[0]}&ids={$identitiesRemoved[1]}&ids={$identitiesRemoved[2]}";
        $this->assertEquals($expectedUri, (string) $request->getUri());
        $this->assertEquals("DELETE", $request->getMethod());
        $this->assertEquals([ "Bearer {$accessToken}" ], $request->getHeader("Authorization"));
    }
}
