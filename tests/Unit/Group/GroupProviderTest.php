<?php

namespace Glance\AuthorizationService\Tests\Unit\Group;

use Glance\AuthorizationService\Group\Category;
use Glance\AuthorizationService\Group\Exception\FailedToAddMembersException;
use Glance\AuthorizationService\Group\Exception\FailedToCreateStaticGroupException;
use Glance\AuthorizationService\Group\Exception\FailedToDeleteGroupException;
use Glance\AuthorizationService\Group\Exception\FailedToFindGroupException;
use Glance\AuthorizationService\Group\Exception\FailedToFindGroupMemberIdentitiesException;
use Glance\AuthorizationService\Group\Exception\FailedToRemoveMembersException;
use Glance\AuthorizationService\Group\Exception\GroupNotFoundException;
use Glance\AuthorizationService\Group\Exception\FailedToFindMemberGroupFromGroupException;
use Glance\AuthorizationService\Group\Exception\FailedToRemoveMemberGroupFromGroupException;
use Glance\AuthorizationService\Group\Exception\FailedToTransferGroupOwnership;
use Glance\AuthorizationService\Group\GroupProvider;
use Glance\AuthorizationService\Group\Group;
use Glance\AuthorizationService\Group\GroupId;
use Glance\AuthorizationService\Group\PrivacyPolicy;
use Glance\AuthorizationService\Group\SelfSubscriptionPolicy;
use Glance\AuthorizationService\Group\SyncType;
use Glance\AuthorizationService\Identity\Identity;
use Glance\AuthorizationService\Identity\IdentityId;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware as GuzzleMiddleware;
use GuzzleHttp\Psr7\Response;
use Http\Message\RequestMatcher\RequestMatcher;
use InvalidArgumentException;
use Nyholm\Psr7\Factory\Psr17Factory;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

final class GroupProviderTest extends TestCase
{
    public function testFindGroupByIdentifier(): void
    {
        $groupData = $this->validGroupArray();

        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $groupData ],
                ])
            ),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $group = $groupProvider->findGroupByIdentifier("fence-developers");

        $this->assertEquals($groupData, $group->toArray());
        $this->assertEquals(1, count($history));
    }

    public function testFindGroupByIdentifierFail(): void
    {
        $groupData = $this->validGroupArray();

        $mock = new MockHandler([ new Response(500, [], '{ "message": "Error message" }') ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $this->expectException(FailedToFindGroupException::class);
        $groupProvider->findGroupByIdentifier("fence-developers");
    }

    public function testFindGroupByIdentifierNotFound(): void
    {
        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 0 ],
                    "data" => [],
                ])
            ),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $this->expectException(GroupNotFoundException::class);
        $groupProvider->findGroupByIdentifier("fence-developers");
    }

    public function testCreateStaticGroup(): void
    {
        $id = "08d771b2-78da-2fc0-fbd3-3816f92282c8";
        $identifier = "test-group";
        $name = "Test Group";
        $description = "Group created for testing reasons";
        $groupData = [
            "id" => $id,
            "groupIdentifier" => $identifier,
            "displayName" => $name,
            "description" => $description,
            "ownerId" => "f41f0213-901b-4fbb-b542-9d32578eaa5f",
            "administratorsId" => null,
            "approvalRequired" => true,
            "selfSubscriptionType" => "Closed",
            "privacyType" => "Admins",
            "dynamic" => false,
            "syncType" => "Master",
            "resourceCategory" => "Test",
        ];

        $mock = new MockHandler([
            new Response(200, [], json_encode([ "data" => $groupData ])),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $group = $groupProvider->createStaticGroup($identifier, $name, $description);

        $this->assertEquals($id, $group->id()->toString());
        $this->assertEquals("test-group", $group->identifier());
        $this->assertEquals(1, count($history));

        /** @var \Psr\Http\Message\RequestInterface $lastRequest */
        $lastRequest = end($history)["request"];

        $this->assertEquals("POST", $lastRequest->getMethod());
        $this->assertEquals(
            [
                "groupIdentifier"      => $identifier,
                "displayName"          => $name,
                "description"          => $description,
                "administratorsId"     => null,
                "expirationDeadline"   => null,
                "resourceCategory"     => $groupData["resourceCategory"],
                "selfSubscriptionType" => $groupData["selfSubscriptionType"],
                "privacyType"          => $groupData["privacyType"],
                "approvalRequired"     => $groupData["approvalRequired"],
                "syncType"             => $groupData["syncType"],
            ],
            json_decode($lastRequest->getBody(), true)
        );
    }

    public function testCreateStaticGroupFail(): void
    {
        $id = "08d771b2-78da-2fc0-fbd3-3816f92282c8";
        $identifier = "test-group";
        $name = "Test Group";
        $description = "Group created for testing reasons";
        $groupData = [
            "id" => $id,
            "groupIdentifier" => $identifier,
            "displayName" => $name,
            "description" => $description,
            "ownerId" => "f41f0213-901b-4fbb-b542-9d32578eaa5f",
            "administratorsId" => null,
            "approvalRequired" => true,
            "selfSubscriptionType" => "Closed",
            "privacyType" => "Admins",
            "dynamic" => false,
            "syncType" => "Master",
            "resourceCategory" => "Test",
        ];

        $mock = new MockHandler([ new Response(500, [], '{ "message": "Error message" }') ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $this->expectException(FailedToCreateStaticGroupException::class);
        $groupProvider->createStaticGroup($identifier, $name, $description);
    }

    public function testUpdateStaticGroup(): void
    {
        $id = "08d771b2-78da-2fc0-fbd3-3816f92282c8";
        $identifier = "test-group";
        $name = "Display name updated";
        $description = "Description updated";
        $groupData = [
            "id" => $id,
            "groupIdentifier" => $identifier,
            "displayName" => $name,
            "description" => $description,
            "administratorsId" => null,
            "approvalRequired" => true,
            "selfSubscriptionType" => "Closed",
            "privacyType" => "Admins",
            "syncType" => "Master",
            "resourceCategory" => "Test",
            "dynamic" => false,
            "ownerId" => "f41f0213-901b-4fbb-b542-9d32578eaa5f",
        ];

        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $this->validGroupArray() ],
                ])
            ),
            new Response(200, [], json_encode([ "data" => $groupData ])),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $group = $groupProvider->updateGroup(
            $id,
            $name,
            $description,
            $groupData["administratorsId"],
            Category::fromString($groupData["resourceCategory"]),
            SelfSubscriptionPolicy::fromString($groupData["selfSubscriptionType"]),
            PrivacyPolicy::fromString($groupData["privacyType"]),
            $groupData["approvalRequired"],
            SyncType::fromString($groupData["syncType"])
        );

        $this->assertEquals($id, $group->id()->toString());
        $this->assertEquals($identifier, $group->identifier());
        $this->assertEquals($name, $group->displayName());
        $this->assertEquals($description, $group->description());
        $this->assertEquals($groupData["resourceCategory"], $group->resourceCategory()->toString());
        $this->assertEquals($groupData["selfSubscriptionType"], $group->selfSubscriptionType()->toString());
        $this->assertEquals($groupData["privacyType"], $group->privacyType()->toString());
        $this->assertEquals($groupData["approvalRequired"], $group->approvalRequired());
        $this->assertEquals($groupData["syncType"], $group->syncType()->toString());
        $this->assertEquals(2, count($history));

        /** @var \Psr\Http\Message\RequestInterface $lastRequest */
        $lastRequest = end($history)["request"];
        $this->assertEquals("PATCH", $lastRequest->getMethod());
    }

    public function testUpdateStaticGroupSyncTypeToInvalidString(): void
    {
        $id = "08d771b2-78da-2fc0-fbd3-3816f92282c8";
        $identifier = "test-group";
        $name = "Display name updated";
        $description = "Description updated";
        $groupData = [
            "id" => $id,
            "groupIdentifier" => $identifier,
            "displayName" => $name,
            "description" => $description,
            "administratorsId" => null,
            "approvalRequired" => true,
            "selfSubscriptionType" => "Closed",
            "privacyType" => "Admins",
            "syncType" => "Potato",
            "resourceCategory" => "Test",
            "dynamic" => false,
            "ownerId" => "f41f0213-901b-4fbb-b542-9d32578eaa5f",
        ];

        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $this->validGroupArray() ],
                ])
            ),
            new Response(200, [], json_encode([ "data" => $groupData ])),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );
        $this->expectException(InvalidArgumentException::class);
        $group = $groupProvider->updateGroup(
            $id,
            $name,
            $description,
            $groupData["administratorsId"],
            Category::fromString($groupData["resourceCategory"]),
            SelfSubscriptionPolicy::fromString($groupData["selfSubscriptionType"]),
            PrivacyPolicy::fromString($groupData["privacyType"]),
            $groupData["approvalRequired"],
            SyncType::fromString($groupData["syncType"])
        );
    }

    public function testDeleteGroup(): void
    {
        $groupData = $this->validGroupArray();

        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $groupData ],
                ])
            ),
            new Response(200),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $groupProvider->deleteGroup("fence-developers");

        $this->assertEquals(2, count($history));

        /** @var \Psr\Http\Message\RequestInterface $lastRequest */
        $lastRequest = end($history)["request"];

        $this->assertEquals("DELETE", $lastRequest->getMethod());
        $this->assertStringContainsString($groupData["id"], $lastRequest->getUri());
    }

    public function testDeleteGroupFail(): void
    {
        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $this->validGroupArray() ],
                ])
            ),
            new Response(400, [], '{"message": "Error message."}'),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $this->expectException(FailedToDeleteGroupException::class);
        $groupProvider->deleteGroup("fence-developers");
    }

    public function testAddMembersToGroup(): void
    {
        $groupIdentifier = "fence-developers";
        $identityId = "59edf297-bd07-4000-904d-80cc6288004a";

        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $this->validGroupArray() ],
                ])
            ),
            new Response(200),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $groupProvider->addMembersToGroup($groupIdentifier, [ IdentityId::fromString($identityId) ]);

        $this->assertEquals(2, count($history));

        /** @var \Psr\Http\Message\RequestInterface $lastRequest */
        $lastRequest = end($history)["request"];

        $this->assertEquals("ids={$identityId}", $lastRequest->getUri()->getQuery());
        $this->assertEquals("POST", $lastRequest->getMethod());
    }
    public function testAddMembersToGroupFail(): void
    {
        $groupIdentifier = "fence-developers";
        $identityId = "59edf297-bd07-4000-904d-80cc6288004a";

        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $this->validGroupArray() ],
                ])
            ),
            new Response(400, [], '{ "message": "Error message" }'),
        ]);

        $handlerStack = HandlerStack::create($mock);
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $this->expectException(FailedToAddMembersException::class);
        $groupProvider->addMembersToGroup($groupIdentifier, [ IdentityId::fromString($identityId) ]);
    }

    public function testRemoveMembersFromGroupFail(): void
    {
        $groupIdentifier = "fence-developers";
        $identityId = "59edf297-bd07-4000-904d-80cc6288004a";

        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $this->validGroupArray() ],
                ])
            ),
            new Response(400, [], '{ "message": "Error message" }'),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $this->expectException(FailedToRemoveMembersException::class);
        $groupProvider->removeMembersFromGroup($groupIdentifier, [ IdentityId::fromString($identityId) ]);
    }

    public function testRemoveMembersFromGroup(): void
    {
        $groupIdentifier = "fence-developers";
        $identityId = "59edf297-bd07-4000-904d-80cc6288004a";

        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $this->validGroupArray() ],
                ])
            ),
            new Response(200),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $groupProvider->removeMembersFromGroup($groupIdentifier, [ IdentityId::fromString($identityId) ]);

        $this->assertEquals(2, count($history));

        /** @var \Psr\Http\Message\RequestInterface $lastRequest */
        $lastRequest = end($history)["request"];

        $this->assertEquals("ids={$identityId}", $lastRequest->getUri()->getQuery());
        $this->assertEquals("DELETE", $lastRequest->getMethod());
    }

    public function testSynchronizeMembers(): void
    {
        $groupData = $this->validGroupArray();
        $groupId = $groupData["id"];
        $groupIdentifier = $groupData["groupIdentifier"];
        $identitiesData = $this->validIdentitiesArray();

        $client = new \Http\Mock\Client();

        $findGroupMembersRequest = new RequestMatcher(
            "api/v1.0/Group/{$groupId}/memberidentities",
            null,
            ["GET"]
        );
        $findGroupMembersResponse = $this->fakeResponseWithBody([
            "data" => [
                $identitiesData[0],
                $identitiesData[1],
            ],
        ]);
        $client->on($findGroupMembersRequest, $findGroupMembersResponse);

        $findGroupRequest = new RequestMatcher("api/v1.0/Group");
        $findGroupResponse = $this->fakeResponseWithBody([
            "pagination" => [ "total" => 1 ],
            "data" => [$groupData],
        ]);
        $client->on($findGroupRequest, $findGroupResponse);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $newComposition = [
            IdentityId::fromString($identitiesData[1]["id"]),
            IdentityId::fromString($identitiesData[2]["id"]),
        ];
        $groupProvider->synchronizeMembers($groupIdentifier, $newComposition);

        $requestHistory = $client->getRequests();
        $addRequest = $requestHistory[3];
        $removeRequest = $requestHistory[5];
        $this->assertEquals("POST", $addRequest->getMethod());
        $this->assertStringContainsString($groupData["id"], $addRequest->getUri());
        $this->assertStringContainsString($identitiesData[2]["id"], $addRequest->getUri());
        $this->assertEquals("DELETE", $removeRequest->getMethod());
        $this->assertStringContainsString($groupData["id"], $removeRequest->getUri());
        $this->assertStringContainsString($identitiesData[0]["id"], $removeRequest->getUri());
        $this->assertEquals(6, count($requestHistory));
    }

    public function testFindMemberIdentitiesFail(): void
    {
        $identitiesData = $this->validIdentitiesArray();
        $groupId = "08d771b2-78da-2fc0-fbd3-3816f92282c8";
        $groupIdentifier = "fence-developers";

        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $this->validGroupArray() ],
                ])
            ),
            new Response(500, [], '{ "message": "Error message." }')
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $this->expectException(FailedToFindGroupMemberIdentitiesException::class);
        $groupProvider->findMemberIdentities($groupIdentifier);
    }

    public function testFindMemberIdentities(): void
    {
        $identitiesData = $this->validIdentitiesArray();
        $groupId = "08d771b2-78da-2fc0-fbd3-3816f92282c8";
        $groupIdentifier = "fence-developers";

        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $this->validGroupArray() ],
                ])
            ),
            new Response(
                200,
                [],
                json_encode([ "data" => $identitiesData ])
            ),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $identities = $groupProvider->findMemberIdentities($groupIdentifier);

        $identitiesArray = array_map(
            function (Identity $identity): array {
                return $identity->toArray();
            },
            $identities
        );
        $this->assertEquals($identitiesData, $identitiesArray);

        $this->assertEquals(2, count($history));

        /** @var \Psr\Http\Message\RequestInterface $lastRequest */
        $lastRequest = end($history)["request"];

        $this->assertEquals(
            "/api/v1.0/Group/{$groupId}/memberidentities",
            $lastRequest->getUri()->getPath()
        );
        $this->assertEquals("GET", $lastRequest->getMethod());
    }

    public function testFindMemberIdentitiesRecursiveFail(): void
    {
        $groupIdentifier = "fence-developers";

        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $this->validGroupArray() ],
                ])
            ),
            new Response(500, [], '{ "message": "Error message." }')
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $this->expectException(FailedToFindGroupMemberIdentitiesException::class);
        $groupProvider->findMemberIdentitiesRecursive($groupIdentifier);
    }

    public function testFindMemberIdentitiesRecursive(): void
    {
        $identitiesData = $this->validIdentitiesArray();
        $groupId = "08d771b2-78da-2fc0-fbd3-3816f92282c8";
        $groupIdentifier = "fence-developers";

        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $this->validGroupArray() ],
                ])
            ),
            new Response(
                200,
                [],
                json_encode([ "data" => $identitiesData ])
            ),
            new Response(
                200,
                [],
                json_encode([ "data" => [] ])
            ),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $identities = $groupProvider->findMemberIdentitiesRecursive($groupIdentifier);

        $identitiesArray = array_map(
            function (Identity $identity): array {
                return $identity->toArray();
            },
            $identities
        );
        $this->assertEquals($identitiesData, $identitiesArray);

        $this->assertEquals(3, count($history));

        /** @var \Psr\Http\Message\RequestInterface $lastRequest */
        $lastRequest = end($history)["request"];

        $this->assertEquals(
            "/api/v1.0/Group/{$groupId}/memberidentities/precomputed",
            $lastRequest->getUri()->getPath()
        );
        $this->assertEquals("GET", $lastRequest->getMethod());
    }

    public function testFindMemberGroupsFromGroup(): void
    {
        $groupsData = $this->validGroupsArray();
        $groupId = "08d771b2-78da-2fc0-fbd3-3816f92282c8";

        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $this->validGroupArray() ],
                ])
            ),
            new Response(
                200,
                [],
                json_encode([ "data" => $groupsData ])
            ),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $groups = $groupProvider->findMemberGroupsFromGroup($groupId);

        $groupsArray = array_map(
            function (Group $group): array {
                return $group->toArray();
            },
            $groups
        );
        $this->assertEquals($groupsData, $groupsArray);

        $this->assertEquals(2, count($history));

        /** @var \Psr\Http\Message\RequestInterface $lastRequest */
        $lastRequest = end($history)["request"];

        $this->assertEquals(
            "/api/v1.0/Group/{$groupId}/membergroups",
            $lastRequest->getUri()->getPath()
        );
        $this->assertEquals("GET", $lastRequest->getMethod());
    }

    public function testFindMemberGroupsFromGroupFail(): void
    {
        $groupsData = $this->validGroupsArray();
        $groupId = "08d771b2-78da-2fc0-fbd3-3816f92282c8";

        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $this->validGroupArray() ],
                ])
            ),
            new Response(500, [], '{ "message": "Error message." }')
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );


        $this->expectException(FailedToFindMemberGroupFromGroupException::class);
        $groupProvider->findMemberGroupsFromGroup($groupId);
    }

    public function testRemoveMemberGroupsFromGroup(): void
    {
        $removedGroupId = "59edf297-bd07-4000-904d-80cc6288004a";
        $groupId = "08da1941-85bb-456d-8c5f-cf308490e073";

        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $this->validGroupArray() ],
                ])
            ),
            new Response(200),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $groupProvider->removeMemberGroupsFromGroup($groupId, [ GroupId::fromString($removedGroupId) ]);

        $this->assertEquals(2, count($history));

        /** @var \Psr\Http\Message\RequestInterface $lastRequest */
        $lastRequest = end($history)["request"];

        $this->assertEquals("ids={$removedGroupId}", $lastRequest->getUri()->getQuery());
        $this->assertEquals("DELETE", $lastRequest->getMethod());
    }

    public function testRemoveMemberGroupsFromGroupFail(): void
    {
        $removedGroupId = "59edf297-bd07-4000-904d-80cc6288004a";
        $groupId = "08da1941-85bb-456d-8c5f-cf308490e073";

        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $this->validGroupArray() ],
                ])
            ),
            new Response(500, [], '{ "message": "Error message." }')
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $this->expectException(FailedToRemoveMemberGroupFromGroupException::class);
        $groupProvider->removeMemberGroupsFromGroup($groupId, [ GroupId::fromString($removedGroupId) ]);
    }

    public function testRemoveAllMembersFromGroup(): void
    {
        $identitiesRemoved = [
            $this->validIdentitiesArray()[0]["id"],
            $this->validIdentitiesArray()[1]["id"],
            $this->validIdentitiesArray()[2]["id"],
        ];
        $groupId = "08d771b2-78da-2fc0-fbd3-3816f92282c8";
        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $this->validGroupArray() ],
                ])
            ),
            new Response(
                200,
                [],
                json_encode([
                    "data" => $this->validIdentitiesArray() ,
                ])
            ),
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $this->validGroupArray() ],
                ])
            ),
            new Response(200),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $groupProvider->removeAllMembersFromGroup($groupId);

        $this->assertEquals(4, count($history));

        /** @var \Psr\Http\Message\RequestInterface $lastRequest */
        $lastRequest = end($history)["request"];

        $this->assertEquals(
            "ids={$identitiesRemoved[0]}"
            . "&ids={$identitiesRemoved[1]}"
            . "&ids={$identitiesRemoved[2]}",
            $lastRequest->getUri()->getQuery()
        );
        $this->assertEquals("DELETE", $lastRequest->getMethod());
    }

    public function testRemoveAllMemberGroupsFromGroup(): void
    {
        $groupsRemoved = [
            $this->validGroupsArray()[0]["id"],
            $this->validGroupsArray()[1]["id"],
        ];
        $groupId = "08d771b2-78da-2fc0-fbd3-3816f92282c8";
        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $this->validGroupArray() ],
                ])
            ),
            new Response(
                200,
                [],
                json_encode([
                    "data" => $this->validGroupsArray() ,
                ])
            ),
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $this->validGroupArray() ],
                ])
            ),
            new Response(200),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $groupProvider->removeAllMemberGroupsFromGroup($groupId);

        $this->assertEquals(4, count($history));

        /** @var \Psr\Http\Message\RequestInterface $lastRequest */
        $lastRequest = end($history)["request"];

        $this->assertEquals(
            "ids={$groupsRemoved[0]}"
            . "&ids={$groupsRemoved[1]}",
            $lastRequest->getUri()->getQuery()
        );
        $this->assertEquals("DELETE", $lastRequest->getMethod());
    }

    public function testRemoveAllMembersAndAllGroupMembersFromGroup(): void
    {
        $identitiesRemoved = [
            $this->validIdentitiesArray()[0]["id"],
            $this->validIdentitiesArray()[1]["id"],
            $this->validIdentitiesArray()[2]["id"],
        ];
        $groupsRemoved = [
            $this->validGroupsArray()[0]["id"],
            $this->validGroupsArray()[1]["id"],
        ];
        $groupId = "08d771b2-78da-2fc0-fbd3-3816f92282c8";
        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $this->validGroupArray() ],
                ])
            ),
            new Response(
                200,
                [],
                json_encode([
                    "data" => $this->validIdentitiesArray() ,
                ])
            ),
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $this->validGroupArray() ],
                ])
            ),
            new Response(200),
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $this->validGroupArray() ],
                ])
            ),
            new Response(
                200,
                [],
                json_encode([
                    "data" => $this->validGroupsArray() ,
                ])
            ),
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $this->validGroupArray() ],
                ])
            ),
            new Response(200),
        ]);
        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [ "client" => $client ]
        );

        $groupProvider->removeAllMembersAndAllGroupMembersFromGroup($groupId);

        $this->assertEquals(8, count($history));

        /** @var \Psr\Http\Message\RequestInterface $firstDeleteRequest */
        $firstDeleteRequest = $history[count($history) - 5]["request"];

        $this->assertEquals(
            "ids={$identitiesRemoved[0]}"
            . "&ids={$identitiesRemoved[1]}"
            . "&ids={$identitiesRemoved[2]}",
            $firstDeleteRequest->getUri()->getQuery()
        );
        $this->assertEquals("DELETE", $firstDeleteRequest->getMethod());

        /** @var \Psr\Http\Message\RequestInterface $secondDeleteRequest */
        $secondDeleteRequest = end($history)["request"];

        $this->assertEquals(
            "ids={$groupsRemoved[0]}"
            . "&ids={$groupsRemoved[1]}",
            $secondDeleteRequest->getUri()->getQuery()
        );
        $this->assertEquals("DELETE", $secondDeleteRequest->getMethod());
    }

    public function testTransferGroupOwnership(): void
    {
        $groupIdentifier = "fence-developers";
        $newOwnerId = "59edf297-bd07-4000-904d-80cc6288004a";

        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [ "total" => 1 ],
                    "data" => [ $this->validGroupArray() ],
                ])
            ),
            new Response(200),
        ]);

        $handlerStack = HandlerStack::create($mock);
        $history = [];
        $handlerStack->push(GuzzleMiddleware::history($history));
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [
                "client" => $client,
            ]
        );

        $groupProvider->transferGroupOwnership($groupIdentifier, $newOwnerId);

        $finalRequest = end($history)["request"];

        $this->assertEquals(2, count($history));
        $this->assertEquals("PUT", $finalRequest->getMethod());
    }

    public function testTransferGroupOwnershipFail(): void
    {
        $groupIdentifier = "fence-developers";
        $newOwnerId = "59edf297-bd07-4000-904d-80cc6288004a";

        $mock = new MockHandler([
            new Response(
                200,
                [],
                json_encode([
                    "pagination" => [
                        "total" => 1,
                    ],
                    "data" => [
                        $this->validGroupArray(),
                    ],
                ])
            ),
            new Response(400, [], '{ "message": "Attenzione Pickpocket!" }'),
        ]);

        $handlerStack = HandlerStack::create($mock);
        $client = new Client([ "handler" => $handlerStack ]);

        $groupProvider = GroupProvider::createWithAccessToken(
            $this->accessToken(),
            [
                "client" => $client,
            ]
        );

        $this->expectException(FailedToTransferGroupOwnership::class);
        try {
            $groupProvider->transferGroupOwnership($groupIdentifier, $newOwnerId);
        } catch (FailedToTransferGroupOwnership $exception) {
            $this->assertStringContainsString("Attenzione Pickpocket!", $exception->getMessage());
            throw $exception;
        }
    }

    private function validGroupsArray(): array
    {
        return [
            [
                "id" => "08d771b2-78da-2fc0-fbd3-3816f92282c8",
                "groupIdentifier" => "fence-developers",
                "displayName" => "fence-developers",
                "description" => "Fence developers",
                "ownerId" => "f41f0213-901b-4fbb-b542-9d32578eaa5f",
                "administratorsId" => "08d779a9-1e2c-72ad-52ff-45ff27e620c1",
                "approvalRequired" => false,
                "selfSubscriptionType" => "Closed",
                "privacyType" => "Members",
                "dynamic" => false,
                "syncType" => "Master",
                "resourceCategory" => "Undefined",
            ],
            [
                "id" => "08da1941-85bb-456d-8c5f-cf308490e073",
                "groupIdentifier" => "cahferre-group",
                "displayName" => "cahferre-group",
                "description" => "My group",
                "ownerId" => "49ae2bc1-80a3-480d-8262-b66b61dad19e",
                "administratorsId" => "08d779aa-7a14-c9dc-a928-53e85188a039",
                "approvalRequired" => false,
                "selfSubscriptionType" => "Closed",
                "privacyType" => "Members",
                "dynamic" => false,
                "syncType" => "Master",
                "resourceCategory" => "Undefined",
            ],
        ];
    }

    private function validGroupArray(): array
    {
        return [
            "id" => "08d771b2-78da-2fc0-fbd3-3816f92282c8",
            "groupIdentifier" => "fence-developers",
            "displayName" => "fence-developers",
            "description" => "Fence developers",
            "ownerId" => "f41f0213-901b-4fbb-b542-9d32578eaa5f",
            "administratorsId" => "08d779a9-1e2c-72ad-52ff-45ff27e620c1",
            "approvalRequired" => false,
            "selfSubscriptionType" => "Closed",
            "privacyType" => "Members",
            "dynamic" => false,
            "syncType" => "Master",
            "resourceCategory" => "Undefined",
        ];
    }

    private function validIdentitiesArray(): array
    {
        return [
            [
                "id"                 => "59edf297-bd07-4000-904d-80cc6288004a",
                "type"               => "Person",
                "upn"                => "mgunters",
                "displayName"        => "Mario Gunter Simao",
                "personId"           => "837034",
                "lastName"           => "Gunter Simao",
                "firstName"          => "Mario",
                "disabled"            => false,
                "blockingReason"     => null,
            ],
            [
                "id"                 => "dbcd6647-fd50-493e-9162-635f355794d4",
                "type"               => "Person",
                "upn"                => "midejesu",
                "displayName"        => "Michelly De Jesus Teixeira",
                "personId"           => "823204",
                "lastName"           => "De Jesus Teixeira",
                "firstName"          => "Michelly",
                "disabled"            => false,
                "blockingReason"     => null,
            ],
            [
                "id"                 => "f41f0213-901b-4fbb-b542-9d32578eaa5f",
                "type"               => "Person",
                "upn"                => "ldomingu",
                "displayName"        => "Leandro Domingues Macedo Alves",
                "personId"           => "755572",
                "lastName"           => "Domingues Macedo Alves",
                "firstName"          => "Leandro",
                "disabled"            => false,
                "blockingReason"     => null,
            ],
        ];
    }

    private function accessToken(): string
    {
        // phpcs:ignore Generic.Files.LineLength.TooLong
        return "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiTWFyaW8ifQ.0_1g4Zit9MJi0-Xc1XYaNEvm1TY9lACgQPq-DUGXMsY";
    }

    private function fakeResponseWithBody(array $body): ResponseInterface
    {
        $factory = new Psr17Factory();
        $fakeBody = $factory->createStream(json_encode($body));

        return $factory->createResponse()->withBody($fakeBody);
    }
}
