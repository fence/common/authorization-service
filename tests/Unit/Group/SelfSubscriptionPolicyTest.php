<?php

namespace Glance\AuthorizationService\Tests\Unit\Group;

use Glance\AuthorizationService\Group\SelfSubscriptionPolicy;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class SelfSubscriptionPolicyTest extends TestCase
{
    public function testFromString(): void
    {
        $selfSubscriptionString = "Open";
        $selfSubscription = SelfSubscriptionPolicy::fromString($selfSubscriptionString);

        $this->assertEquals($selfSubscriptionString, $selfSubscription->toString());
    }

    public function testFromInvalidString(): void
    {
        $invalidSelfSubscriptionPolicy = "Potato";

        $this->expectException(InvalidArgumentException::class);
        SelfSubscriptionPolicy::fromString($invalidSelfSubscriptionPolicy);
    }

    public function testClosed(): void
    {
        $selfSubscription = SelfSubscriptionPolicy::closed();

        $this->assertEquals("Closed", $selfSubscription->toString());
    }

    public function testOpen(): void
    {
        $selfSubscription = SelfSubscriptionPolicy::open();

        $this->assertEquals("Open", $selfSubscription->toString());
    }

    public function testCernUsers(): void
    {
        $selfSubscription = SelfSubscriptionPolicy::cernUsers();

        $this->assertEquals("CernUsers", $selfSubscription->toString());
    }
}
