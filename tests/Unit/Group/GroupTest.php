<?php

namespace Glance\AuthorizationService\Tests\Unit\Group;

use Glance\AuthorizationService\Group\Group;
use PHPUnit\Framework\TestCase;

final class GroupTest extends TestCase
{
    public function testArrayManipulation(): void
    {
        $input = $this->validGroupArray();

        $group = Group::fromArray($input);

        $this->assertSame($input, $group->toArray());
        $this->assertSame($input["id"], $group->id()->toString());
        $this->assertSame($input["groupIdentifier"], $group->identifier());
        $this->assertSame($input["displayName"], $group->displayName());
        $this->assertSame($input["description"], $group->description());
        $this->assertSame($input["ownerId"], $group->ownerId()->toString());
        $this->assertSame($input["administratorsId"], $group->administratorsId()->toString());
        $this->assertSame($input["approvalRequired"], $group->approvalRequired());
        $this->assertSame(
            $input["selfSubscriptionType"],
            $group->selfSubscriptionType()->toString()
        );
        $this->assertSame($input["privacyType"], $group->privacyType()->toString());
        $this->assertSame($input["dynamic"], $group->dynamic());
        $this->assertSame($input["syncType"], $group->syncType()->toString());
        $this->assertSame($input["resourceCategory"], $group->resourceCategory()->toString());
    }

    private function validGroupArray(): array
    {
        return [
            "id" => "08d771b2-78da-2fc0-fbd3-3816f92282c8",
            "groupIdentifier" => "fence-developers",
            "displayName" => "fence-developers",
            "description" => "Fence developers",
            "ownerId" => "f41f0213-901b-4fbb-b542-9d32578eaa5f",
            "administratorsId" => "08d779a9-1e2c-72ad-52ff-45ff27e620c1",
            "approvalRequired" => false,
            "selfSubscriptionType" => "Closed",
            "privacyType" => "Members",
            "dynamic" => false,
            "syncType" => "Master",
            "resourceCategory" => "Undefined",
        ];
    }
}
