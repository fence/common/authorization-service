<?php

namespace Glance\AuthorizationService\Tests\Unit\Group;

use Glance\AuthorizationService\Group\SyncType;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class SyncTypeTest extends TestCase
{
    public function testFromString(): void
    {
        $selfSubscriptionString = "Master";
        $selfSubscription = SyncType::fromString($selfSubscriptionString);

        $this->assertEquals($selfSubscriptionString, $selfSubscription->toString());
    }

    public function testMaster(): void
    {
        $selfSubscription = SyncType::master();

        $this->assertEquals("Master", $selfSubscription->toString());
    }

    public function testReplica(): void
    {
        $selfSubscription = SyncType::replica();

        $this->assertEquals("Replica", $selfSubscription->toString());
    }

    public function testNoSync(): void
    {
        $selfSubscription = SyncType::noSync();

        $this->assertEquals("NoSync", $selfSubscription->toString());
    }

    public function testSyncError(): void
    {
        $selfSubscription = SyncType::syncError();

        $this->assertEquals("SyncError", $selfSubscription->toString());
    }
}
