<?php

namespace Glance\AuthorizationService\Tests\Unit\Group;

use Glance\AuthorizationService\Group\Category;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class CategoryTest extends TestCase
{
    public function testFromString(): void
    {
        $categoryString = "Official";
        $category = Category::fromString($categoryString);

        $this->assertEquals($categoryString, $category->toString());
    }

    public function testFromInvalidString(): void
    {
        $invalidCategory = "Potato";

        $this->expectException(InvalidArgumentException::class);
        Category::fromString($invalidCategory);
    }

    public function testTest(): void
    {
        $category = Category::test();

        $this->assertEquals("Test", $category->toString());
    }

    public function testOfficial(): void
    {
        $category = Category::official();

        $this->assertEquals("Official", $category->toString());
    }

    public function testUndefined(): void
    {
        $category = Category::undefined();

        $this->assertEquals("Undefined", $category->toString());
    }

    public function testPersonal(): void
    {
        $category = Category::personal();

        $this->assertEquals("Personal", $category->toString());
    }
}
