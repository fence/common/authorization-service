<?php

namespace Glance\AuthorizationService\Tests\Unit\Group;

use Glance\AuthorizationService\Group\PrivacyPolicy;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class PrivacyPolicyTest extends TestCase
{
    public function testFromString(): void
    {
        $privacyPolicyString = "Open";
        $privacyPolicy = PrivacyPolicy::fromString($privacyPolicyString);

        $this->assertEquals($privacyPolicyString, $privacyPolicy->toString());
    }

    public function testFromInvalidString(): void
    {
        $invalidPrivacyPolicy = "Potato";

        $this->expectException(InvalidArgumentException::class);
        PrivacyPolicy::fromString($invalidPrivacyPolicy);
    }

    public function testGroupAdministrators(): void
    {
        $privacyPolicy = PrivacyPolicy::groupAdministrators();

        $this->assertEquals("Admins", $privacyPolicy->toString());
    }

    public function testOpen(): void
    {
        $privacyPolicy = PrivacyPolicy::open();

        $this->assertEquals("Open", $privacyPolicy->toString());
    }

    public function testMembers(): void
    {
        $privacyPolicy = PrivacyPolicy::members();

        $this->assertEquals("Members", $privacyPolicy->toString());
    }
}
