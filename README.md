# Authorization Service

This is an SDK to use [CERN Authorization Service API](https://authorization-service-api.web.cern.ch/swagger/index.html).

## Installation

Install using Composer:

```bash
composer require glance-project/authorization-service
```

## Getting started

To use this library, you might need to have an application registered on
[Application Portal](https://application-portal.web.cern.ch). If you do not
know what this means, refer to the [CERN Authorization Service documentation](https://auth.docs.cern.ch/applications/application-configuration)

With your application registered, you will use the **Client ID** and **Client secret**.

## Usage

This library was designed to be used through several providers.

All providers can be created with application credentials or with an access token.

```php
// Example of $inProduction. Adapt for your needs!
$inProduction = getenv("ENVIRONMENT") === "PRODUCTION";

$groupProvider = GroupProvider::createWithAppCredentials($clientId, $clientSecret, $inProduction);

$identityProvider = IdentityProvider::createWithAccessToken("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUziJ9...", $inProduction);
```

### GroupProvider

#### `GroupProvider::findGroupByIdentifier()`

Find a group by the identifier.

A `GroupNotFoundException` is thrown if the group does not exist.

```php
$group = $groupProvider->findGroupByIdentifier("fence-developers");

$group->identifier();     // "fence-developers"
$group->id()->toString(); // "08d771b2-78da-2fc0-fbd3-3816f92282c8"
```

#### `GroupProvider::createStaticGroup()`

Create a static group. Only the identifier, display name and description are required.

```php
$group = $groupProvider->createStaticGroup(
    "test-group",                                                // Identifier           (required)
    "A test group",                                              // Display name         (required)
    "This is a test group description",                          // Description          (required)
    GroupId::fromString("08d779a9-1e2c-72ad-52ff-45ff27e620c1"), // Administrators group (optional)
    Category::test(),                                            // Category             (optional)
    new DateTimeImmutable("2021-12-31"),                         // Expiration date      (optional)
    SelfSubscriptionPolicy::closed(),                            // Self subscription    (optional)
    PrivacyPolicy::groupAdministrators(),                        // Privacy policy       (optional)
    true                                                         // Approval required    (optional)
);
```

#### `GroupProvider::deleteGroup()`

Delete a group by the identifier.

```php
$groupProvider->deleteGroup("fence-developers");
```

#### `GroupProvider::synchronizeMembers()`

Define the composition of a group using *Identity IDs*.

Members witch are not present on the given array of *Identity IDs* will be
removed.

At the end, only the informed members will be part of the group.

```php
$identityIds = $identityProvider->getIdsFromPersonIds([837034, 123456]);

$groupProvider->synchronizeMembers("fence-developers", $identityIds);

$members = $groupProvider->findMemberIdentities("fence-developers");

foreach ($members as $member) {
    echo $member->personId(); . "\n"
}

// Output:
// 837034
// 123456
```

#### `GroupProvider::addMembersToGroup()`

Add members to group without changing the whole group composition.

```php
$identityIds = $identityProvider->getIdsFromPersonIds([837034, 123456]);

$groupProvider->addMembersToGroup("fence-developers", $identityIds);
```

#### `GroupProvider::removeMembersFromGroup()`

Remove members from group without changing the whole group composition.

```php
$identityIds = $identityProvider->getIdsFromPersonIds([ 837034 ]);

$groupProvider->removeMembersFromGroup("fence-developers", $identityIds);
```

#### `GroupProvider::findMemberIdentities()`

Find members which are inside group. Returns an array of `Identity` objects.

```php
$members = $groupProvider->findMemberIdentities("fence-developers");

foreach ($members as $member) {
    $member->personId();
    $member->displayName();
}
```

#### `GroupProvider::findMemberGroupsFromGroup()`
An exception will be thrown if there is no matching groupId or if the method fails.

```php
$groups = $groupProvider->findMemberGroupsFromGroup("59edf297-bd07-4000-904d-80cc6288004a");

foreach ($groups as $group) {
    echo $group->toString() . "\n";
}

// Output:
// 59edf297-bd07-4000-904d-80cc6288004a
// dbcd6647-fd50-493e-9162-635f355794d4
```

#### `GroupProvider::removeMemberGroupsFromGroup()`
An exception will be thrown if there is no matching groupId or if the method fails.

```php
$groupProvider->removeMemberGroupsFromGroup("59edf297-bd07-4000-904d-80cc6288004a");

```

#### `GroupProvider::removeAllMembersFromGroup()`
An exception will be thrown if there is no matching groupId or if the method fails.

```php
$groupProvider->removeAllMembersFromGroup("59edf297-bd07-4000-904d-80cc6288004a");

```

#### `GroupProvider::removeAllMemberGroupsFromGroup()`
An exception will be thrown if there is no matching groupId or if the method fails.

```php
$groupProvider->removeAllMemberGroupsFromGroup("59edf297-bd07-4000-904d-80cc6288004a");

```

#### `GroupProvider::removeAllMembersAndAllGroupMembersFromGroup()`
An exception will be thrown if there is no matching groupId or if the method fails.

```php
$groupProvider->removeAllMembersAndAllGroupMembersFromGroup("59edf297-bd07-4000-904d-80cc6288004a");

```
### IdentityProvider

#### `IdentityProvider::getIdsFromPersonIds()`

Get *Identity IDs* from given *Person IDs*.

When it is not possible to get a Identity ID, a `FailedToFindIdentityIdException` is thrown.

If you want a result with only the found IDs, refer to `IdentityProvider::findIdsFromPersonIds()`.

```php
$ids = $identityProvider->getIdsFromPersonIds([837034, 823204]);

foreach ($ids as $id) {
    echo $id->toString() . "\n";
}

// Output:
// 59edf297-bd07-4000-904d-80cc6288004a
// dbcd6647-fd50-493e-9162-635f355794d4
```

#### `IdentityProvider::findIdsFromPersonIds()`

Finds *Identity IDs* from given *Person IDs*.

When it is not possible to get a Identity ID, it is skipped and only the found IDs are returned.

If you want a method which throws an exception, refer to `IdentityProvider::getIdsFromPersonIds()`.

```php
$ids = $identityProvider->findIdsFromPersonIds([837034, 123456]); // 123456 does not exist

foreach ($ids as $id) {
    echo $id->toString() . "\n";
}

// Output:
// 59edf297-bd07-4000-904d-80cc6288004a
```

#### `IdentityProvider::searchByName()`

```php
$identities = $identityProvider->searchByName("Mar");

foreach ($identities as $identity) {
    echo $identity->displayName() . "\n";
}

// Output:
// Mario Gunter Simao
// Marcelo Teixeira dos Santos
```

#### `IdentityProvider::findByPersonId()`

An exception will be thrown if there is no matching personId or if the method fails.

```php
$identity = $identityProvider->findByPersonId(837034);

echo $identity->displayName();

// Output:
// Mario Gunter Simao
```

#### `IdentityProvider::findByIdentityId()`
An exception will be thrown if there is no matching identityId or if the method fails.

```php
$identity = $identityProvider->findByIdentityId(" 59edf297-bd07-4000-904d-80cc6288004a");

echo $identity->displayName();

// Output:
// Mario Gunter Simao
```